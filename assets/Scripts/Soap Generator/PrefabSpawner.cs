﻿/*
 * Author: Ulf Simonsen, Georgi Muerov
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// The spawner component. 
/// Creates soaps and throws them at the player.
/// </summary>
public class PrefabSpawner : MonoBehaviour
{

    public GameObject target;
    public GameObject thrower;

    /// <summary>
    /// The time between spawning new objects.
    /// </summary>
    private float spawnTime;
    /// <summary>
    /// The target variance in the x axis of the target.
    /// </summary>
    public float xVariance;
    /// <summary>
    /// The lower bound of the rotation speed of the spawned prefab.
    /// </summary>
    public float rotationSpeedFrom;
    /// <summary>
    /// The upper bound of the rotation speed of the spawned prefab.
    /// </summary>
    public float rotationSpeedTo;
    /// <summary>
    /// The initial prefab that is spawned.
    /// </summary>
    public GameObject startPrefab;
    /// <summary>
    /// Occurs when prefab spawns.
    /// </summary>
    public delegate void Spawned();
    public event Spawned PrefabSpawn;

    /// <summary>
    /// Signifies if the spawner is spawning.
    /// </summary>
    private bool spawning;
    /// <summary>
    /// The current number of prefabs on screen.
    /// </summary>
    private int prefabsOnScreen = 0;

    private bool recentlySpawned = false;

    /// <summary>
    /// The maximum number of prefabs on screen.
    /// </summary>
    private int maximumNumberOfPrefabs;

    public bool showDebugOutput = false;

    private Dictionary<GameObject, int> spawns = new Dictionary<GameObject, int>();

    public void OnDifficultyChange(DifficultyData data)
    {
        this.maximumNumberOfPrefabs = data.maximumSoaps;
        this.spawns = data.spawns;
        this.spawnTime = data.spawnRate;
    }

    // Use this for initialization
    void Start()
    {
        DifficultyManager manager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<DifficultyManager>();
        manager.DifficultyChanged += new DifficultyManager.DifficultyChangedHandler(OnDifficultyChange);

        GameState.Instance.SoapChainExplosion += SoapExplode;
        StartCoroutine(Spawner());

        //Spawn the object
        GameObject minion = (GameObject) Instantiate(startPrefab);

        minion.tag = "FirstSoap";
        SetRandomRotation((SoapRotation) minion.GetComponent<SoapRotation>());

        //Listen to the death of the object
        minion.GetComponent<Destructible>().PrefabDestroyed += SoapDeath;

        //Initial position
        minion.transform.position = GameObject.Find("FirstSoapSpawner").transform.position;

        minion.GetComponent<SoapSlowfall>().SetActive(true);
        minion.GetComponent<DestructibleClicked>().givePoints = false; // Don't give points on the first click
        minion.rigidbody.useGravity = false;

        this.prefabsOnScreen = 1;
    }

    // Update is called once per frame
    void Update()
    {

    }

    /// <summary>
    /// Raises the prefab spawn event.
    /// </summary>
    void OnPrefabSpawn()
    {
        this.prefabsOnScreen += 1;
        if(PrefabSpawn != null)
        {
            PrefabSpawn();
        }
    }

    /// <summary>
    /// Returns a GameObject based on random choice from the spawns dictionary.
    /// </summary>
    private GameObject GetSpawn()
    {
        int sumOfWeights = 0;
        foreach(KeyValuePair<GameObject, int> entry in this.spawns)
        {
            sumOfWeights += entry.Value;
        }
        int choice = UnityEngine.Random.Range(0, sumOfWeights);
        int curSum = 0;
        foreach(KeyValuePair<GameObject, int> entry in this.spawns)
        {
            if(choice <= (entry.Value + curSum))
            {
                if(this.showDebugOutput)
                    Debug.Log("Chose " + entry.Key + " as " + choice + " <= " + (entry.Value + curSum));
                return entry.Key;
            }
            curSum += entry.Value;
        }

        // This should never happen! But... Just in case.
        return this.spawns.Keys.GetEnumerator().Current;
    }

    /// <summary>
    /// Repeatedly spawns prefabs and throws them towards the spawnTo object.
    /// Aims randomly slightly to the side of the object. 
    /// </summary>
    IEnumerator Spawner()
    {
        //Spawns objects  to spawnTo at y axis position spawnHeigthl, every spawnTime interval
        while(true)
        {
            if(GameState.Instance.tutorialPassed && spawning && (maximumNumberOfPrefabs > this.prefabsOnScreen))
            {
                if (!recentlySpawned)
                {
                    if(this.showDebugOutput)
                        Debug.Log("SPAWN");
                    
                    StartCoroutine(SpawnSinglePrefab());
                    
                    if(this.showDebugOutput)
                        Debug.Log("Waiting for " + spawnTime);
                }
                else
                    recentlySpawned = false;

                yield return new WaitForSeconds(spawnTime);
            }
            else
                yield return new WaitForEndOfFrame();
        }
    }

    /// <summary>
    /// Calculates the velocity to be applied to the object.
    /// </summary>
    /// <returns>The velocity vector.</returns>
    /// <param name="from">The place where the object will be launched from</param>
    /// <param name="to">The place where the object should land.</param>
    /// <param name="angle">The angle of initial launch.</param>

    //TODO: Fix accuracy on y axis.

    Vector3 GetVelocityByAngle(Vector3 from, Vector3 to, float angle)
    {
        //Calculate the direction of the throw
        Vector3 dir = to - from;
        float height = dir.y;
        Vector3 XZDirection = new Vector3(dir.x, 0, dir.z);
        float dist = XZDirection.magnitude;
        float radianAngle = angle * Mathf.Deg2Rad;
        dir = new Vector3(dir.x, dist * Mathf.Tan(radianAngle), dir.z);

        //Set the power of the throw
        dist += 1.5f * height / Mathf.Tan(radianAngle);
        float vel = Mathf.Sqrt(dist * Physics.gravity.magnitude / Mathf.Sin(2 * radianAngle));
        dir.Normalize();

        return vel * dir;
    }


    /// <summary>
    /// Gives the spawned prefab a random rotation.
    /// </summary>
    /// <param name="rotation">The constant rotation of the prefab</param>

    void SetRandomRotation(SoapRotation rotation)
    {
        rotation.rotationVector = new Vector3(Random.Range(0f, 1f),
                                              Random.Range(0f, 1f),
                                              Random.Range(0f, 1f));
        rotation.rotationSpeed = Random.Range(rotationSpeedFrom, rotationSpeedTo);
    }

    /// <summary>
    /// Subscribed to the explosion of soaps.
    /// </summary>

    void SoapExplode(SoapExplosionInfo explosionInfo)
    {
        prefabsOnScreen = prefabsOnScreen - explosionInfo.totalDestroyedSoaps - explosionInfo.totalExplodedSoaps;
        CheckForSpawn();
    }

    /// <summary>
    /// Subscribed to the death of a soap.
    /// </summary>

    void SoapDeath()
    {
        prefabsOnScreen--;
        CheckForSpawn();
    }

    public void SetMaximumPrefabs(int num)
    {
        this.maximumNumberOfPrefabs = num;
    }

    /// <summary>
    /// Tells the spawner to begin(resume) spawning.
    /// </summary>
    public void StartSpawning()
    {
        spawning = true;
    }

    public void StopSpawning()
    {
        spawning = false;
    }


    /// <summary>
    /// Checks if there are no more soaps left on the screen. 
    /// If needed, spawns a prefab immediately.
    /// </summary>
    void CheckForSpawn ()
    {
        
        if(this.showDebugOutput)
            Debug.Log("Prefabs on screen: " + prefabsOnScreen);

        if (prefabsOnScreen == 0)
        {
            recentlySpawned = true;

            if(this.showDebugOutput)
                Debug.Log("No soaps on screen, spawning new one.");

            SpawnSinglePrefab();

        }
    }

    /// <summary>
    /// Spawns one prefab.
    /// </summary>

    IEnumerator SpawnSinglePrefab()
    {
        OnPrefabSpawn();
        GameObject minion = (GameObject) Instantiate(this.GetSpawn());

        //Listen to the death of the object
        minion.GetComponent<Destructible>().PrefabDestroyed += SoapDeath;

        //Initial position
        Vector3 localPosition = minion.transform.position;
        Quaternion localRotation = minion.transform.rotation;
     	//Vector3 localScale = minion.transform.localScale;

        minion.transform.parent = transform;
        minion.transform.localPosition = localPosition;
        minion.transform.localRotation = localRotation;
        minion.transform.localScale = new Vector3(0.15f, 0.15f, 0.15f);

        //Set scale in han

        minion.rigidbody.useGravity = false;

        yield return new WaitForSeconds(0.85f); //sorry about magic, this is the lenght of the clip
        throwPrefab(minion);
        //minion.GetComponent<SoapSlowfall>().SetActive(true);
    }

    void throwPrefab(GameObject prefab)
    {
        prefab.transform.parent = null;
        prefab.rigidbody.constraints = RigidbodyConstraints.None;
        float variance = Random.Range(-xVariance, xVariance);

        Vector3 to = new Vector3(target.transform.position.x + variance,
                                 target.transform.position.y,
                                 target.transform.position.z);

        Vector3 velocity = GetVelocityByAngle(transform.position, to, 60f);
        prefab.rigidbody.useGravity = true;
        prefab.rigidbody.velocity = velocity;
        SetRandomRotation((SoapRotation) prefab.GetComponent<SoapRotation>());
		GameState.Instance.OnSoapThrown ();
    }

    //HACK: unset the recently spawned variable to avoid too long waiting times.
    IEnumerator UnsetRecentSpawn()
    {
        yield return new WaitForSeconds(1f);
        recentlySpawned = false;
    }
}
