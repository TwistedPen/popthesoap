﻿/*
 * Author: Georgi Muerov, Thias Borgholm
 */
using UnityEngine;
using System.Collections;

/// <summary>
/// Intended for a "wall" object above the camera.
/// Makes the soap fall down, fixing its rotation, if enabled.
/// </summary>

public class MakeSoapFall : MonoBehaviour 
{
    private float scale = 0.05f;

	/// <summary>
	/// Signifies if the wall should stop the rotation
	/// of soaps hitting it.
	/// </summary>
	public bool stopRotation;

    /// <summary>
    /// Triggers when the wall is hit.
    /// </summary>
    /// <param name="col">The collider of the hit.</param>

    void OnTriggerEnter(Collider col)
    {
        col.gameObject.rigidbody.velocity = Vector3.zero;
        col.gameObject.rigidbody.constraints = RigidbodyConstraints.FreezePositionZ;
        col.gameObject.GetComponent<SoapSlowfall>().SetActive(true);
        col.gameObject.transform.localScale = new Vector3(scale, scale, scale);
		if (stopRotation) {
			// return to initial rotation values
			col.gameObject.transform.rotation = Quaternion.Euler(90,0,0);
            col.gameObject.GetComponent<SoapSlowfall>().SetActive(true);
			// stops the rotation of the soap
			SoapRotation colRotation = col.gameObject.GetComponent<SoapRotation>();
			colRotation.rotationVector = Vector3.zero;
		}
    }
}
