﻿using UnityEngine;
using System.Collections;

public class RandomPointMethod : IThrowingHandler {
    public Vector3 GetVelocity(Vector3 from, ThrowingHandlerOptions opts) {
        Bounds bounds = opts.gameObject.GetComponent<MeshRenderer>().bounds;
        float midX = bounds.min.x + ((bounds.max.x-bounds.min.x) / 2.0f);

        float sign = 0f;
        if (from.x > midX) {
            sign = -1f;
        } else {
            sign = 1f;
        }

		if(UnityEngine.Random.Range(1,10) <= 3) {
			float x = UnityEngine.Random.Range(0, 2) * sign;
			float y = UnityEngine.Random.Range(opts.minY, opts.maxY);
			return new Vector3 (x, y, 0);
		} else {
			float x = UnityEngine.Random.Range(opts.minX, opts.maxX) * sign;
			float y = UnityEngine.Random.Range(opts.minY, opts.maxY);
			return new Vector3 (x, y, 0);
		}
        
    }
}