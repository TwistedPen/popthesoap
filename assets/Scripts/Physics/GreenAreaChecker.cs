﻿/*
 * Author: Wojciech Terepeta
 */
using UnityEngine;
using System.Collections;

public class GreenAreaChecker : Singleton<GreenAreaChecker>
{
    public enum DetectionMethod
    {
        SphereCollider,
        SoapTouching,
        //Green field should be bigger if the below method should be used
        SoapFullyContained
    };

    public DetectionMethod detectionMethod;

    private Bounds bounds;


    void Start()
    {
        var bottom = GameObject.FindGameObjectWithTag("BottomArea");
        this.bounds = bottom.GetComponent<BoxCollider>().bounds;
    }

    public bool ObjectInArea(GameObject obj)
    {
        return this.ObjectInArea(obj, this.detectionMethod);
    }

    public bool ObjectInArea(GameObject obj, DetectionMethod method)
    {
        Bounds objBounds = obj.GetComponent<SphereCollider>().bounds;
        Bounds meshBounds = obj.GetComponent<MeshRenderer>().bounds;

        if(!bounds.Intersects(objBounds))
        {
            return false;
        }

        switch(method)
        {
            case DetectionMethod.SoapTouching:
                if(meshBounds.min.y <= bounds.max.y)
                {
                    return true;
                }
                break;

            case DetectionMethod.SphereCollider:
                return true;

            case DetectionMethod.SoapFullyContained:
                if(meshBounds.max.y <= bounds.max.y)
                {
                    return true;
                }
                break;

            default:
                break;
        }

        return false;
    }
}
