﻿using UnityEngine;
using System.Collections;

public class ThrowingHandlerOptions : MonoBehaviour {
    public float minAngle;
    public float maxAngle;
    public bool isRandomAngle;
    public float randomDestModifier;
    public float xOffset;

    public float minX;
    public float maxX;
    public float minY;
    public float maxY;
}