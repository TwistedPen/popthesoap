﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 
/// </summary>
public class FindPointMethod : IThrowingHandler {
    private float angle;
    private float minAngle;
    private float maxAngle;
    private bool isRandomAngle;

    public FindPointMethod(float minAngle, float maxAngle, bool isRandomAngle) {
        this.minAngle = minAngle;
        this.maxAngle = maxAngle;
        this.angle = this.getRandomAngle();
        this.isRandomAngle = isRandomAngle;
    }

    private float getRandomAngle() {
        return UnityEngine.Random.Range(this.minAngle, this.maxAngle);
    }

    // http://answers.unity3d.com/questions/145972/how-to-make-enemy-canon-ball-fall-on-mooving-targe.html
    private Vector3 Get2DVelocityByAngle(Vector3 from, Vector3 to, float angle)
    {
        Vector3 dir = to - from;
        float h = dir.y;
        dir = new Vector3(dir.x, 0, dir.z);
        float dist = dir.magnitude;
        float a = angle * Mathf.Deg2Rad;
        dir = new Vector3(dir.x, dist * Mathf.Tan(a));
        dist += h / Mathf.Tan(a);
        float vel = Mathf.Sqrt(dist * Physics.gravity.magnitude / Mathf.Sin(2 * a));
        dir.Normalize();
        return vel * dir;
    }

    public Vector3 GetVelocity(Vector3 from, ThrowingHandlerOptions opts) {
        if (this.isRandomAngle)
            this.angle = this.getRandomAngle();

        Bounds bounds = opts.gameObject.GetComponent<MeshRenderer>().bounds;
        float midX = bounds.min.x + ((bounds.max.x-bounds.min.x) / 2.0f);

        Vector3 to = new Vector3(0,0,0);
        float newX = 0f;
        if (from.x > midX) {
            newX = Mathf.Max(bounds.min.x, from.x - opts.xOffset);
        } else {
            newX = Mathf.Min (bounds.max.x, from.x + opts.xOffset);
        }

        to = new Vector3(newX, opts.gameObject.transform.position.y, from.z);
        Debug.DrawLine (from, to);
        //bounds.Contains(new Vector3 (from.x, from.y, opts.gameObject.transform.position.z));
        return this.Get2DVelocityByAngle(from, to, this.angle);
    }
}