﻿using UnityEngine;
using System.Collections;

interface IThrowingHandler {
    Vector3 GetVelocity(Vector3 from, ThrowingHandlerOptions opts);
}