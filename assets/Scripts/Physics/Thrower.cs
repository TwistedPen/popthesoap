﻿/*
 * Author: Martin Grunbaum, Henrik Geertsen, Wojciech Terepeta, Ulf Simonsen, Georgi Muerov
 */
using UnityEngine;
using System.Collections;

public class Thrower : MonoBehaviour
{
    private IThrowingHandler throwHandlerImpl;
    public enum ThrowMethod
    {
        FindPoint,
        Random
    };


    public ThrowMethod throwMethod;

    // Use this for initialization
    void Start()
    {
        GameState.Instance.DestructibleClicked += new GameState.DestructibleClickedHandler(GameObjectClicked);

        if(this.throwMethod == ThrowMethod.FindPoint)
        {
            float minAngle = this.GetComponent<ThrowingHandlerOptions>().minAngle;
            float maxAngle = this.GetComponent<ThrowingHandlerOptions>().maxAngle;
            bool isRandomAngle = this.GetComponent<ThrowingHandlerOptions>().isRandomAngle;
            this.throwHandlerImpl = new FindPointMethod(minAngle, maxAngle, isRandomAngle);
        }
        else if(this.throwMethod == ThrowMethod.Random)
        {
            this.throwHandlerImpl = new RandomPointMethod();
        }
    }

    public void GameObjectClicked(GameObject obj)
    {
        //Check if soap is falling
        if(!obj.GetComponent<ThrownRecently>().IsTrue())
        {
            if(obj.CompareTag("FirstSoap"))
            {
                GameState.Instance.SetPause(false);
                GameState.Instance.tutorialPassed = true;
                obj.tag = "Untagged";
                obj.rigidbody.useGravity = true;
            }
            //The soap is "touching" the green field
            if(GreenAreaChecker.Instance.ObjectInArea(obj))
            {
                Vector3 newVelocity =
                        this.throwHandlerImpl.GetVelocity(obj.transform.position,
                                                      this.GetComponent<ThrowingHandlerOptions>());
                obj.rigidbody.velocity = newVelocity;
                obj.GetComponent<ThrownRecently>().SetThrown();
            }
        }
    }
}
