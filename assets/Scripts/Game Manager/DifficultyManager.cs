﻿/*
 * Author: Georgi Muerov, Henrik Geertsen
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

/// <summary>
/// Manages the changing of difficulty.
/// </summary>
public class DifficultyManager : MonoBehaviour
{
    public delegate void DifficultyChangedHandler(DifficultyData data);
    public event DifficultyChangedHandler DifficultyChanged;

    private Timer timer;
    private float lastChange = 0f;

    bool IsTimeForNextLevel()
    {
        return (this.timer.GetTime() - this.lastChange > this.durationForLevel[this.currentDifficulty])
                        && this.currentDifficulty < this.GetNumLevels() - 1;
    }

    void Update()
    {
        if(GameState.Instance.tutorialPassed && this.IsTimeForNextLevel())
        {
            this.SetDifficulty(this.currentDifficulty + 1);
            Debug.Log("Changing difficulty to: " + currentDifficulty);
            this.lastChange = this.timer.GetTime();
        }
    }

    void Awake()
    {
        var json = JSON.Parse(this.levelsFile.text);
        int curLevel = 0;
        this.numLevels = json.AsArray.Count;
        this.spawnRateForLevel = new float[this.numLevels];
        this.durationForLevel = new int[this.numLevels];
        this.maximumSoapsInLevel = new int[this.numLevels];
		this.fallspeedForLevel = new float[this.numLevels];
		this.fallspeedSmallForLevel = new float[this.numLevels];
        this.spawnsForLevel = new Dictionary<GameObject, int>[this.numLevels];

        foreach(JSONNode node in json.AsArray)
        {
            JSONClass curObj = node.AsObject;
            foreach(KeyValuePair<System.String, JSONNode> e in curObj)
            {
                if(e.Key.Equals("maxSoaps"))
                {
                    this.maximumSoapsInLevel[curLevel] = e.Value.AsInt;
                }
                else if(e.Key.Equals("duration"))
                {
                    this.durationForLevel[curLevel] = e.Value.AsInt;
                }
                else if(e.Key.Equals("spawnRate"))
                {
                    this.spawnRateForLevel[curLevel] = e.Value.AsFloat;
                }
                else if(e.Key.Equals("spawnChances"))
                {
                    this.spawnsForLevel[curLevel] = this.ParseSpawnChances(e.Value);
                }
				else if(e.Key.Equals("fallspeed"))
				{
					this.fallspeedForLevel[curLevel] = e.Value.AsFloat;
				}
				else if(e.Key.Equals("fallspeedSmall"))
				{
					this.fallspeedSmallForLevel[curLevel] = e.Value.AsFloat;
				}
                else
                {
                    Debug.Log("Unknown key:" + e.Key);
                }
            }
            curLevel++;
        }
    }

    void Start()
    {
        this.timer = this.GetComponent<Timer>();
        this.SetDifficulty(0);
    }

    private DifficultyData CreateData(int diff)
    {
        DifficultyData data = new DifficultyData(diff, this.maximumSoapsInLevel[diff],
                                                 this.spawnRateForLevel[diff],
                                                     this.durationForLevel[diff],
		                                         this.fallspeedForLevel[diff],
		                                         this.fallspeedSmallForLevel[diff],
                                                 this.spawnsForLevel[diff]);
        return data;
    }

    public void DecreaseDifficulty()
    {
        if(this.currentDifficulty > 0)
        {
            this.SetDifficulty(this.currentDifficulty - 1);
        }
    }

    public void SetDifficulty(int diff)
    {
        this.currentDifficulty = diff;
        DifficultyData data = this.CreateData(diff);
        if(this.DifficultyChanged != null)
            this.DifficultyChanged(data);
    }

    private int currentDifficulty = 0;
    /// <summary>
    /// The maximum soaps in level.
    /// </summary>
    private int[] maximumSoapsInLevel;
    /// <summary>
    /// The seconds until the level is increased.
    /// </summary>
    private int[] durationForLevel;
    /// <summary>
    /// The spawn rate for the levels.
    /// </summary>
    private float[] spawnRateForLevel;

    private int numLevels = 0;

    private Dictionary<GameObject, int>[] spawnsForLevel;

	private float[] fallspeedForLevel;

	private float[] fallspeedSmallForLevel;

    public TextAsset levelsFile;

	public int GetCurrentDifficulty() {
		return this.currentDifficulty;
	}

	public float GetFallspeed(int level) {
		return this.fallspeedForLevel [level];
	}

	public float GetFallspeedSmall(int level) {
		return this.fallspeedSmallForLevel [level];
    }

    public int GetNumLevels()
    {
        return this.numLevels;
    }

    public int GetMaxSoaps(int level)
    {
        return this.maximumSoapsInLevel[Mathf.Min(level, this.numLevels)];
    }

    public int GetDuration(int level)
    {
        return this.durationForLevel[Mathf.Min(level, this.numLevels)];
    }

    public float GetSpawnRate(int level)
    {
        return this.spawnRateForLevel[Mathf.Min(level, this.numLevels)];
    }

    public Dictionary<GameObject, int> GetSpawns(int level)
    {
        return this.spawnsForLevel[Mathf.Min(level, this.numLevels)];
    }

    private Dictionary<GameObject, int> ParseSpawnChances(JSONNode node)
    {
        Dictionary<GameObject, int> retVal = new Dictionary<GameObject, int>();
        foreach(KeyValuePair<System.String, JSONNode> e in node.AsObject)
        {
            string prefabPath = e.Key;
            GameObject gObj = (GameObject) Resources.Load(prefabPath);
            int spawnChance = e.Value.AsInt;
            //Debug.Log("Adding " + gObj + " as int " + spawnChance);
            retVal.Add(gObj, spawnChance);
        }
        return retVal;
    }
}