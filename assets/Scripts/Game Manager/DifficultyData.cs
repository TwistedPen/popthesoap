﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DifficultyData {
	public readonly int maximumSoaps;
	public readonly int difficulty;
	public readonly float spawnRate;
	public readonly float duration;
	public readonly float fallspeed;
	public readonly float fallspeedSmall;
	public readonly Dictionary<GameObject, int> spawns;

	public DifficultyData(int difficulty, int maxSoaps, float spawnRate, float duration, 
	                      float fallspeed, float fallspeedSmall, Dictionary<GameObject, int> spawns)
	{
		this.maximumSoaps = maxSoaps;
		this.difficulty = difficulty;
		this.spawnRate = spawnRate;
		this.duration = duration;
		this.spawns = spawns;
		this.fallspeed = fallspeed;
		this.fallspeedSmall = fallspeedSmall;
	}
}