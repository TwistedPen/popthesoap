﻿/*
 * Author: Martin Grunbaum, Wojciech Terepeta, Georgi Muerov, Henrik Geertsen, Thais Thomas Borgholm, Ulf Simonsen
 */
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// The main game manager class, taking care of the current lives
/// </summary>
public class GameState : Singleton<GameState>
{
    public bool isDebugBuild = false;
    public bool tutorialPassed = false;
    private DifficultyManager difficultyManager;
    private Timer timerSinceLevelStart;
    private PrefabSpawner spawner;

    #region Events and related properties
    public delegate void PauseHandler(bool state);
    public event PauseHandler Pause;

    public delegate void DecayStatusHandler(int status, GameObject obj);
    public event DecayStatusHandler DecayStatus;

    public delegate void ThrowSoapHandler();
    public event ThrowSoapHandler ThrowSoap;
    
    /// <summary>
    /// The number of lives from the beginning of the game.
    /// </summary>
    public int maxLives;

    private bool isPaused;
    /// <summary>
    /// Returns the state of the game, whether it's paused or not.
    /// </summary>
    public bool IsPaused
    {
        get
        {
            return this.isPaused;
        }
        private set
        {
            this.isPaused = value;

            if(this.Pause != null)
            {
                this.Pause(value);
            }
        }
    }

    public delegate void LifeHandler(int livesLeft);
    public event LifeHandler Life;

    public delegate void GameOverHandler();
    public event GameOverHandler GameOver;

    private int livesLeft;
    /// <summary>
    /// Yields the current number of lives left.
    /// </summary>
    public int LivesLeft
    {
        get
        {
            return this.livesLeft;
        }
        private set
        {
            this.livesLeft = value;

            if(this.Life != null)
            {
                this.Life(value);
            }

            // Check for gameover.
            if(this.livesLeft <= 0 && this.GameOver != null)
            {
                this.GameOver();
                PlayerPrefs.SetString ("timeLasted", this.timerSinceLevelStart.GetTime ().ToString ("F2"));
                PlayerPrefs.SetInt ("isResult",1);
                Application.LoadLevel ("Menus");
            }
        }
    }

    public delegate void SoapChainExplosionHandler(SoapExplosionInfo explosionInfo);
    public event SoapChainExplosionHandler SoapChainExplosion;

    public delegate void DestructibleClickedHandler(GameObject destructible);
    public event DestructibleClickedHandler DestructibleClicked;

    public delegate void SoapSingleExplosionHandler(Vector3 position, float diameter, LinkedList<SoapDecay> destroyedList);
    public event SoapSingleExplosionHandler SoapSingleExplosion;

    public delegate void SoapDestroyedHandler(Vector3 position);
    public event SoapDestroyedHandler SoapDestroyed;

    public delegate void BackgroundAnimationHandler(int animation);
    public event BackgroundAnimationHandler BackgroundAnimation;
    #endregion


    /// <summary>
    /// Called when a soap is thrown
    /// </summary>
    public void OnSoapThrown()
    {
        if(this.ThrowSoap != null)
        {
            this.ThrowSoap();
        }
    }

    /// <summary>
    /// Called when a destructible object (e.g. soap) is clicked.
    /// </summary>
    /// <param name="destructible">The clicked object.</param>
    public void OnDestructibleClicked(GameObject destructible)
    {
        if(this.DestructibleClicked != null)
        {
            this.DestructibleClicked(destructible);
        }
    }



    void Awake()
    {
        this.timerSinceLevelStart = this.GetComponent<Timer>();
        this.spawner = GameObject.FindGameObjectWithTag("Spawner").GetComponent<PrefabSpawner>();
        this.difficultyManager = this.GetComponent<DifficultyManager>();
        this.timerSinceLevelStart.StartTimer();
        this.LivesLeft = this.maxLives;
    }

    void Start()
    {
        PointSystem.Reset();
        spawner.StartSpawning();
    }

    /// <summary>
    /// Sends the time lasted
    /// </summary>
    public Timer GetTimeLasted()
    {
        return this.timerSinceLevelStart;
        }

    /// <summary>
    /// Called when receiving an event from the explosion, destroying the soaps.
    /// </summary>
    /// <param name="explosionInfo">Detailed information about the explosion.</param>
    public void OnSoapChainExplosion(SoapExplosionInfo explosionInfo)
    {
        if(this.SoapChainExplosion != null)
        {
            SoapChainExplosion(explosionInfo);
        }
    }

    public void OnSoapSingleExplosion(Vector3 position, float diameter, LinkedList<SoapDecay> destroyedList)
    {
        if(this.SoapSingleExplosion != null)
        {
            SoapSingleExplosion(position, diameter, destroyedList);
        }
    }

    public void OnSoapDestroyed(Vector3 position)
    {
        if(this.SoapDestroyed != null)
        {
            SoapDestroyed(position);
        }
    }
    /// <summary>
    /// Called when the animation of the background guys is changed
    /// </summary>
    public void OnBackgroundAnimationChange(int animation)
    {
        if(this.BackgroundAnimation != null)
        {
            BackgroundAnimation(animation);
        }
    }

    /// <summary>
    /// Called when receiving an event when a soap is lost (dropped on the floor).
    /// </summary>
    public void OnSoapLost()
    {
        this.LivesLeft -= 1;
        this.difficultyManager.DecreaseDifficulty();
    }

    /// <summary>
    /// Called when receiving an event the soap decay and the soap is changing size.
    /// </summary>
    /// <param name="status">The status of the soap decays:
    /// 0 = big
    /// 1 = medium
    /// 2 = small
    /// 3 = exploded/gone.
    /// </param>
    public void OnDecayStatusChange(int status, GameObject obj)
    {
        if(this.DecayStatus != null)
        {
            this.DecayStatus(status, obj);
        }
    }

    /// <summary>
    /// Updates the pause state based on the current time scale and fixed delta time.
    /// </summary>
    public void UpdatePause()
    {
        if(Time.timeScale <= FloatingPoint.epsilon && Time.fixedDeltaTime <= FloatingPoint.epsilon)
        {
            GameState.Instance.IsPaused = true;
            this.spawner.StopSpawning();
        }
        else
        {
            GameState.Instance.IsPaused = false;
            this.spawner.StartSpawning();
        }
    }

    /// <summary>
    /// Sets the pause state.
    /// </summary>
    /// <param name="pause">The new pause state.</param>
    public void SetPause(bool pause)
    {
        if(pause != this.IsPaused)
        {
            // TimeUtility will call UpdatePause
            if(pause)
            {
                TimeUtility.SetTimeScale(0f, 0f);
            }
            else
            {
                TimeUtility.RestoreTimeScale();
            }
        }
    }

    /// <summary>
    /// Toggles the pause state.
    /// </summary>
    public bool TogglePause()
    {
        if(!this.IsPaused)
        {
            this.SetPause(true);
            return true;
        }
        else
        {
            this.SetPause(false);
            return false;
        }
    }

    Rect pauseRect = new Rect(0, 0, 100, 50);
    Rect restartRect = new Rect(100, 0, 100, 50);
    Rect elapsedRect = new Rect(10, 60, 100, 50);
    Rect livesRect = new Rect(10, 100, 100, 50);
    Rect pointsRect = new Rect(10, 120, 100, 50);
    Rect normalSoapsDestroyedRect = new Rect(10, 160, 100, 50);
    Rect redSoapsDestroyedRect = new Rect(10, 200, 100, 50);
    Rect greenSoapsDestroyedRect = new Rect(10, 240, 100, 50);
//    Rect gameOverRect = new Rect(Screen.width/2-200, Screen.height/2-50, 400, 100);

    // TODO: change this (remove?)
    void OnGUI()
    {
        if(this.isDebugBuild) 
        {
            if(this.LivesLeft > 0)
            {
                if(GUI.Button(pauseRect, "Pause"))
                {
                    this.TogglePause();
                }
            }
            else
            {
                HighScoreChecker.CheckForHighscore(PointSystem.TotalPoints);
                //GUI.Label(gameOverRect, "<color=black><size=40>Game Over!</size></color>");
            }

            if(GUI.Button(restartRect, "Restart"))
            {
                Application.LoadLevel("DefaultScene");
            }

            GUI.Label(elapsedRect, "Elapsed time: " + this.timerSinceLevelStart.GetTime());
            GUI.Label(livesRect, "Lives: " + this.LivesLeft);
            GUI.Label(pointsRect, "Points: " + PointSystem.TotalPoints);
            GUI.Label(normalSoapsDestroyedRect, "Normal soaps: " + PointSystem.SoapsDestroyedByType[SoapType.Type.Normal]);
            GUI.Label(redSoapsDestroyedRect, "Red soaps: " + PointSystem.SoapsDestroyedByType[SoapType.Type.Red]);
            GUI.Label(greenSoapsDestroyedRect, "Green soaps: " + PointSystem.SoapsDestroyedByType[SoapType.Type.Green]);
        }
    }

    void OnDestroy()
    {
        if(IsPaused)
        {
            TimeUtility.ResetTimeScale();
        }
    }
}
