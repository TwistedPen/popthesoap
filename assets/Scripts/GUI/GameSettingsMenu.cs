using UnityEngine;
using System.Collections;

public class GameSettingsMenu : MonoBehaviour {

	public static void DisplayGameSettingsMenu() {

	int edgePaddingSide = MenuManager.edgePaddingSide;
	int edgePaddingTop = MenuManager.edgePaddingTop;
	int spaceBetweenButtons = MenuManager.spaceBetweenButtons;
	int newWidth = Screen.width - edgePaddingSide * 2;
	int newHeight = Screen.height - edgePaddingTop * 2;

	int centerPointButtonX = (newWidth-MenuManager.buttonsWidth)/2;
		//Make a group on the center of the screen
    GUI.BeginGroup(new Rect(0, MenuManager.groupY, newWidth, newHeight));
		
	if (GUI.Button (new Rect (centerPointButtonX, MenuManager.scaleHeight(30), MenuManager.buttonsWidth, MenuManager.buttonsHeight),LanguageDictionary.GetLanguageString("audioSettings"))) {
			MenuManager.menuBehaviour = AudioSettingsMenu.DisplayAudioMenu;
		}
		
	if (GUI.Button (new Rect (centerPointButtonX, MenuManager.scaleHeight(30)+spaceBetweenButtons, MenuManager.buttonsWidth, MenuManager.buttonsHeight), LanguageDictionary.GetLanguageString("languageSettings"))) {
			MenuManager.menuBehaviour = LanguageSettingsMenu.DisplayLanguageMenu;
		}
		
	if (GUI.Button (new Rect (centerPointButtonX, MenuManager.scaleHeight(30)+spaceBetweenButtons*4, MenuManager.buttonsWidth, MenuManager.buttonsHeight), LanguageDictionary.GetLanguageString("back"))) {
			MenuManager.menuBehaviour = MainMenu.DisplayMainMenu;
		}
		
		GUI.EndGroup ();
	
	}
}
