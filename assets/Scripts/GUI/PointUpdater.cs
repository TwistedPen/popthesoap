﻿/*
 * Author: Martin Grunbaum, Wojciech Terepeta
 */
using UnityEngine;
using System.Collections;

public class PointUpdater : MonoBehaviour
{
    public GUIStyle pointsStyle;
    public float countingTime = 1f;
    public float interval = 0.05f;

    private PointSystem pointSystem;
    private int pointsToCount = 0;
    private int pointChunk;
    private TextMesh textMesh;
	private Vector3 origPosition;

	// Use this for initialization
	void Awake() {
		this.pointSystem = GameObject.FindGameObjectWithTag("GameManager").GetComponent<PointSystem>();
		this.pointSystem.PointsChanged += new PointSystem.PointsChangedHandler (OnPointsChanged);
		this.textMesh = this.GetComponent<TextMesh>();
		this.origPosition = this.transform.position;
	}

    void Start()
    {
        InvokeRepeating("UpdateInterval", 0, this.interval);
    }

	void SetPosition(int count) {
		this.textMesh.transform.position = new Vector3(
			origPosition.x - (count*0.85f), 
			origPosition.y, 
			origPosition.z);
	}

	void UpdateInterval() {
		if(this.pointsToCount > 0) {
			int pointsToUpdate = (this.pointChunk > this.pointsToCount ? this.pointsToCount : this.pointChunk);
			int prevLen = this.textMesh.text.Length;
			this.textMesh.text = (int.Parse(this.textMesh.text) + pointsToUpdate).ToString();
            this.pointsToCount -= pointsToUpdate;
			if(this.textMesh.text.Length > prevLen) {
				this.SetPosition(this.textMesh.text.Length);
			}
		}
	}

    public void OnPointsChanged(int pts, Vector3? vect)
    {
        this.pointsToCount += pts;
        this.pointChunk = (int) (this.pointsToCount / this.countingTime * this.interval);

        if(this.pointChunk <= 0)
        {
            this.pointChunk += 1;
        }
    }
}
