﻿using UnityEngine;
using System.Collections;

public class PauseButton : MonoBehaviour {
	public Rect positionRect;
	public Texture2D icon;
	public GUIStyle btnStyle;

	void OnGUI() {
		if(GUI.Button(new Rect(MenuManager.scaleWidth((int)this.positionRect.x),MenuManager.scaleWidth((int)this.positionRect.y),
		                       MenuManager.scaleWidth((int)this.positionRect.width),MenuManager.scaleWidth((int)this.positionRect.height)), icon, btnStyle)) {
			if(GameState.Instance.TogglePause() == false) {
				this.icon = (Texture2D) Resources.Load("PauseBtn");
			} else {
				this.icon = (Texture2D) Resources.Load("PlayBtn");
			}
		}
	}
}
