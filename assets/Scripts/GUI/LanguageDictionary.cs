﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

public class LanguageDictionary : MonoBehaviour {

	public static  int selectedLanguage = 0;														//The selected language of the game. Default to English (0) -- also supports danish (1)
	public static  TextAsset entry = (TextAsset)Resources.Load("json", typeof(TextAsset));			//The JSON file with the dictionary
	public static  JSONNode languageDictionary = JSONNode.Parse(entry.text);						//The dictionary with English and Danish support for the gamelanguageDictionary  //The parsed JSON string

	public static string GetLanguageString(string word){
		return languageDictionary[word][selectedLanguage];
	}

}
