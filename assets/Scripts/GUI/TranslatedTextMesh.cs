﻿using UnityEngine;
using System.Collections;

public class TranslatedTextMesh : MonoBehaviour
{
    void Start()
    {
        this.GetComponent<TextMesh>().text = LanguageDictionary.GetLanguageString("tapTheSoap");
    }
}
