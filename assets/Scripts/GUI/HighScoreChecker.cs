﻿using UnityEngine;
using System.Collections;

public class HighScoreChecker : MonoBehaviour {

	public static int positionPointer;

	public static bool CheckForHighscore(int score){

		if (InsertedToHighScoreTable (score)) {

			HighScoreChecker.SortPlayersPrefs ();						//Empty the appropriate highScore
			HighScoreChecker.InsertHighScore (PointSystem.TotalPoints);	//Insert the new one
			return true;
		}
		return false;
	}
	/// <summary>
	/// Checks if the new score is to be inserted in the highscore and at what position
	/// </summary>
	/// <returns><c>true</c>, if to high score table was inserted, <c>false</c> otherwise.</returns>
	/// <param name="score">Score.</param>
	public static bool InsertedToHighScoreTable(int score){

		int[] highScores = new int[10];
		int highScore;
		string highScoreKey;
		bool inserted = false;		//Boolean to see if the score is high enough to enter the leaderboard
		positionPointer = 1;
		for (int i = 0; i < highScores.Length; i++) {
			//Get the highScore from 1 - 10
			highScoreKey = "HighScore" + (i + 1).ToString ();
			highScore = PlayerPrefs.GetInt(highScoreKey);

			if(score<highScore){
				positionPointer++;
			}else{
				inserted=true;
				break;
			}
		}
		return inserted;
	}

	/// <summary>
	/// Moves every highscore from the stored positions on spot down and removes the lowest score
	/// </summary>
	public static void SortPlayersPrefs(){

		int highScore;
		string highScoreKey;
		string newHighScoreKey;

		for (int i = 10; i > positionPointer; i--) {

			highScoreKey = "HighScore" + (i - 1).ToString ();

			newHighScoreKey = "HighScore" + (i).ToString ();

			highScore = PlayerPrefs.GetInt(highScoreKey);


			PlayerPrefs.SetInt (newHighScoreKey,highScore);	

		}
	}

	// Insert the score at the stored position in the list
	public static void InsertHighScore(int score){

		string highScoreKey;

		highScoreKey = "HighScore" + (positionPointer).ToString ();

		PlayerPrefs.SetInt (highScoreKey,score);

	}

}
