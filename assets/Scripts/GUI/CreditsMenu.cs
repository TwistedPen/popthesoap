﻿using UnityEngine;
using System.Collections;

public class CreditsMenu : MonoBehaviour {

	public static Vector2 scrollPosition = Vector2.zero;
	static Touch touch;

	static GUIStyle textStyle;
	static GUIStyle thxStyle;

	public static void setStyle(GUIStyle style,GUIStyle style2){
		textStyle = style;
		thxStyle = style2;
	}

	public static void DisplayCreditsMenu() {
			
		int edgePaddingSide = MenuManager.edgePaddingSide;
		int edgePaddingTop = MenuManager.edgePaddingTop;
		int spaceBetweenButtons = MenuManager.spaceBetweenButtons-MenuManager.scaleHeight(10);

		int scrollDepth = MenuManager.scaleHeight(3000);
		int newWidth = Screen.width - edgePaddingSide * 2;
		int newHeight = Screen.height - edgePaddingTop * 2;
		int mainMenuWidth = 400;

		//Make a group for the buttons
        GUI.BeginGroup(new Rect(0, MenuManager.groupY, newWidth, newHeight));

        CreditsMenu.scrollPosition = GUI.BeginScrollView(new Rect(MenuManager.scaleWidth(10), MenuManager.scaleHeight(20), MenuManager.scaleWidth(850), newHeight -MenuManager.scaleWidth(400)), 
		                                                          scrollPosition, new Rect(0,0, MenuManager.scaleWidth(400), scrollDepth),GUIStyle.none,GUIStyle.none);


//		Make a group on the center of the screen
		GUI.BeginGroup (new Rect (0, 0, newWidth, scrollDepth));

		int textPaddingLeft = MenuManager.scaleWidth (60);
		int textPaddingTop = MenuManager.scaleHeight (5);
		int textWidth = MenuManager.scaleWidth (50);
		//Finds the center point on the x-axis for the buttons
		int centerPointButtonX = (newWidth-MenuManager.buttonsWidth)/2-MenuManager.scaleWidth(10);

		GUI.Label (new Rect (textPaddingLeft, textPaddingTop, mainMenuWidth, textWidth), LanguageDictionary.GetLanguageString ("gameDirector") + "\n Niels Elmvig Bruhn", textStyle);
		GUI.Label (new Rect (textPaddingLeft, textPaddingTop + spaceBetweenButtons, mainMenuWidth, textWidth), LanguageDictionary.GetLanguageString ("gameDesigner") + "\n Miki Anthony", textStyle);
		GUI.Label (new Rect (textPaddingLeft, textPaddingTop + spaceBetweenButtons * 2f, mainMenuWidth, textWidth), LanguageDictionary.GetLanguageString ("levelDesigner") + "\n Lau Nielsen", textStyle);
		GUI.Label (new Rect (textPaddingLeft, textPaddingTop + spaceBetweenButtons * 3f, mainMenuWidth, textWidth), LanguageDictionary.GetLanguageString ("leadProgrammer") + "\n Thomas K. Malowanczyk", textStyle);
		GUI.Label (new Rect (textPaddingLeft, textPaddingTop + spaceBetweenButtons * 4f, mainMenuWidth, textWidth), LanguageDictionary.GetLanguageString ("programmers") + "\n Martin Grunbaum", textStyle);
		GUI.Label (new Rect (textPaddingLeft, textPaddingTop + spaceBetweenButtons * 5f, mainMenuWidth, textWidth), " Wojciech Terepeta", textStyle);
		GUI.Label (new Rect (textPaddingLeft, textPaddingTop + spaceBetweenButtons * 5.5f, mainMenuWidth, textWidth), " Dimitrios Tsirozoglou", textStyle);
		GUI.Label (new Rect (textPaddingLeft, textPaddingTop + spaceBetweenButtons * 6f, mainMenuWidth, textWidth), " Ulf Gaarde Simonsen", textStyle);
		GUI.Label (new Rect (textPaddingLeft, textPaddingTop + spaceBetweenButtons * 6.5f, mainMenuWidth, textWidth), " Georgi Muerov", textStyle);
		GUI.Label (new Rect (textPaddingLeft, textPaddingTop + spaceBetweenButtons * 7.5f, mainMenuWidth, textWidth), LanguageDictionary.GetLanguageString ("artDirector") + "\n Glenn Lange", textStyle);
		GUI.Label (new Rect (textPaddingLeft, textPaddingTop + spaceBetweenButtons * 8.55f, mainMenuWidth, textWidth), LanguageDictionary.GetLanguageString ("visualDesigner") + "\n Dilan Yuksel", textStyle);
		GUI.Label (new Rect (textPaddingLeft, textPaddingTop + spaceBetweenButtons * 9.5f, mainMenuWidth, textWidth), LanguageDictionary.GetLanguageString ("animator") + "\n Marie-Louise Hedegaard", textStyle);
		GUI.Label (new Rect (textPaddingLeft, textPaddingTop + spaceBetweenButtons * 10.5f, mainMenuWidth, textWidth), LanguageDictionary.GetLanguageString ("CgArtist") + "\n Nick Jakobsen", textStyle);
		GUI.Label (new Rect (textPaddingLeft, textPaddingTop + spaceBetweenButtons * 11.5f, mainMenuWidth, textWidth), " Nichlas Petersen", textStyle);
		GUI.Label (new Rect (textPaddingLeft, textPaddingTop + spaceBetweenButtons * 12.5f, mainMenuWidth, textWidth), LanguageDictionary.GetLanguageString ("audioDesigner") + "\n Henrik Hedegaard", textStyle);
		GUI.Label (new Rect (textPaddingLeft, textPaddingTop + spaceBetweenButtons * 13.5f, mainMenuWidth, textWidth), LanguageDictionary.GetLanguageString ("pipelineProgrammer") + "\n Thais Borgholm", textStyle);
		GUI.Label (new Rect (textPaddingLeft, textPaddingTop + spaceBetweenButtons * 14.5f, mainMenuWidth, textWidth), LanguageDictionary.GetLanguageString ("qaProgrammer") + "\n Henrik Geertsen", textStyle);
		GUI.Label (new Rect (textPaddingLeft, textPaddingTop + spaceBetweenButtons * 15.5f, mainMenuWidth, textWidth), LanguageDictionary.GetLanguageString ("leaderQa") + "\n Nanna F. Topp", textStyle);
		GUI.Label (new Rect (textPaddingLeft, textPaddingTop + spaceBetweenButtons * 16.5f, mainMenuWidth, textWidth), LanguageDictionary.GetLanguageString ("qa") + "\n Andreas Dalsaa", textStyle);
		GUI.Label (new Rect (textPaddingLeft, textPaddingTop + spaceBetweenButtons * 17.5f, mainMenuWidth, textWidth), LanguageDictionary.GetLanguageString ("testers") + "\n Dennis F. Hansen", textStyle);
		GUI.Label (new Rect (textPaddingLeft, textPaddingTop + spaceBetweenButtons * 18.5f, mainMenuWidth, textWidth), "Espen Jensen", textStyle);
		GUI.Label (new Rect (textPaddingLeft, textPaddingTop + spaceBetweenButtons * 19.5f, mainMenuWidth, textWidth), "Lasse Rasmussen", textStyle);
		GUI.Label (new Rect (textPaddingLeft, textPaddingTop + spaceBetweenButtons * 20f, mainMenuWidth, textWidth), "Martin Nejsum", textStyle);
		GUI.Label (new Rect (textPaddingLeft, textPaddingTop + spaceBetweenButtons * 20.5f, mainMenuWidth, textWidth),"Nicolai V. Døssing", textStyle);
		GUI.Label (new Rect (textPaddingLeft, textPaddingTop + spaceBetweenButtons * 21.5f, mainMenuWidth, textWidth), LanguageDictionary.GetLanguageString ("projectManager") + "\n Kimie Bodin Rygar ", textStyle);
		GUI.Label (new Rect (textPaddingLeft-15, textPaddingTop + spaceBetweenButtons * 22.5f, mainMenuWidth, textWidth), LanguageDictionary.GetLanguageString ("creditsThx1"), thxStyle);
		GUI.Label (new Rect (textPaddingLeft, textPaddingTop + spaceBetweenButtons * 23f, mainMenuWidth, textWidth), LanguageDictionary.GetLanguageString ("creditsThx2"), thxStyle);

		if (GUI.Button (new Rect (centerPointButtonX, textPaddingTop + spaceBetweenButtons * 24f, MenuManager.buttonsWidth, MenuManager.buttonsHeight), LanguageDictionary.GetLanguageString ("back"))) {
			MenuManager.menuBehaviour = MainMenu.DisplayMainMenu;
		}
		GUI.EndGroup ();
		GUI.EndScrollView ();
		GUI.EndGroup();

		if (Input.touchCount > 0) {
			touch = Input.touches [0];
			if (touch.phase == TouchPhase.Moved) {
				scrollPosition.y += touch.deltaPosition.y/2;
			}
		}
	}

}
