﻿using UnityEngine;
using System.Collections;

public class LeaderBoardMenu : MonoBehaviour {

	public static Vector2 scrollPosition = Vector2.zero;

	static GUIStyle textStyle;
	
	public static void setStyle(GUIStyle style){
		textStyle = style;
	}

	public static void DisplayLeaderBoardMenu() {

		int edgePaddingSide = MenuManager.edgePaddingSide;
		int edgePaddingTop = MenuManager.edgePaddingTop;
		int spaceBetweenButtons = MenuManager.spaceBetweenButtons;
		int productionMenuHeight = 215;							//The height of the production menu box
		
		int newWidth = Screen.width - edgePaddingSide * 2;
		int newHeight = Screen.height - edgePaddingTop * 2;
		
		//Make a group for the buttons
        GUI.BeginGroup(new Rect(0, MenuManager.groupY, newWidth, newHeight));
		
		//Finds the center point on the x-axis for the buttons
		int centerPointButtonX = (newWidth-MenuManager.buttonsWidth)/2;

		//int highScoreWidth = MenuManager.scaleWidth ();
		int scoreDistance = MenuManager.scaleHeight (40);

//		LeaderBoardMenu.scrollPosition = GUI.BeginScrollView(new Rect(10, Screen.height/2-180, 400, Screen.height/2+70), scrollPosition, new Rect(0,0, 355, 600));

		int[] highScores = new int[10];
		int highScore;
		string highScoreKey;

		GUI.Label (new Rect (centerPointButtonX+MenuManager.scaleWidth(10)+MenuManager.scaleWidth(70), MenuManager.scaleHeight(28), MenuManager.buttonsWidth, MenuManager.buttonsHeight), LanguageDictionary.GetLanguageString("leaderBoardT") , textStyle);

		for (int i = 1; i <= 10; i++) {

			//Get the highScore from 1 - 10 -- The highest is 1!
			highScoreKey = "HighScore" + i.ToString ();
			highScore = PlayerPrefs.GetInt(highScoreKey);
			if(i < 10)
				GUI.Label (new Rect (centerPointButtonX+MenuManager.scaleWidth(10), MenuManager.scaleHeight(35)+scoreDistance*(i), MenuManager.buttonsWidth, MenuManager.buttonsHeight ),"  "+i.ToString() + ".    " + highScore, textStyle);
			else
				GUI.Label (new Rect (centerPointButtonX+MenuManager.scaleWidth(10), MenuManager.scaleHeight(35)+scoreDistance*(i), MenuManager.buttonsWidth, MenuManager.buttonsHeight ),i.ToString() + ".    " + highScore, textStyle);
		}

		if (GUI.Button (new Rect (centerPointButtonX, MenuManager.scaleHeight(30)+spaceBetweenButtons*4, MenuManager.buttonsWidth, MenuManager.buttonsHeight), LanguageDictionary.GetLanguageString("back"))) {
			MenuManager.menuBehaviour = MainMenu.DisplayMainMenu;
		}
		GUI.EndGroup();
//		GUI.EndScrollView();
	}
}
