﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

//Implementation of the Screen Displayed when the player Wins!
public class ResultsMenu : MonoBehaviour {

	static GUIStyle textStyle;
	
	public static void setStyle(GUIStyle style){
		textStyle = style;
	}

	public static void DisplayResultsMenu() {
		int edgePaddingSide = MenuManager.edgePaddingSide;
		int edgePaddingTop = MenuManager.edgePaddingTop;
		int spaceBetweenButtons = MenuManager.spaceBetweenButtons;
		int newWidth = Screen.width - edgePaddingSide * 2;
		int newHeight = Screen.height - edgePaddingTop * 2;

		int centerPointButtonX = (newWidth-MenuManager.buttonsWidth)/2;
		
		//Make a group on the center of the screen
        GUI.BeginGroup(new Rect(0, MenuManager.groupY, newWidth, newHeight));

		//Make a label to display player's Score
		GUI.Label (new Rect (centerPointButtonX, MenuManager.scaleHeight(30), MenuManager.buttonsWidth, MenuManager.buttonsHeight),LanguageDictionary.GetLanguageString("scored") + PointSystem.TotalPoints + LanguageDictionary.GetLanguageString(" points"), textStyle);

		//Make a label to display player time lasted
		GUI.Label (new Rect (centerPointButtonX, MenuManager.scaleHeight(30)+spaceBetweenButtons/2, MenuManager.buttonsWidth, MenuManager.buttonsHeight), LanguageDictionary.GetLanguageString("timeResult") + PlayerPrefs.GetString ("timeLasted") + LanguageDictionary.GetLanguageString("time"), textStyle);

		//Make a label to display Soaps of type1
		//GUI.Label (new Rect (centerPointButtonX, MenuManager.scaleHeight(30)+spaceBetweenButtons*1, MenuManager.buttonsWidth, MenuManager.buttonsHeight),LanguageDictionary.GetLanguageString("type1") + "  X  " + PointSystem.SoapsDestroyedByType[SoapType.Type.Normal],textStyle);

		//Make a label to display Soaps of type2
		//GUI.Label (new Rect (centerPointButtonX, MenuManager.scaleHeight(30)+spaceBetweenButtons*1.5f, MenuManager.buttonsWidth, MenuManager.buttonsHeight),LanguageDictionary.GetLanguageString("type2") + "  X  " + PointSystem.SoapsDestroyedByType[SoapType.Type.Red],textStyle);

		//Make a label to display Soaps of type3
		//GUI.Label (new Rect (centerPointButtonX, MenuManager.scaleHeight(30)+spaceBetweenButtons*2, MenuManager.buttonsWidth, MenuManager.buttonsHeight),LanguageDictionary.GetLanguageString("type3") + "  X  " + PointSystem.SoapsDestroyedByType[SoapType.Type.Green],textStyle);

		//Add Buttons for game navigation
		if (GUI.Button (new Rect (centerPointButtonX, MenuManager.scaleHeight(30)+spaceBetweenButtons*3f, MenuManager.buttonsWidth, MenuManager.buttonsHeight), LanguageDictionary.GetLanguageString("playAgain"))) {
			Application.LoadLevel(1);				//Load the Starting Level
		}

		if (GUI.Button (new Rect (centerPointButtonX, MenuManager.scaleHeight(30)+spaceBetweenButtons*4f, MenuManager.buttonsWidth, MenuManager.buttonsHeight),LanguageDictionary.GetLanguageString("backToMenu"))) {
			PlayerPrefs.Save();
			MenuManager.menuBehaviour = MainMenu.DisplayMainMenu;

		}

		GUI.EndGroup();
	}

}
