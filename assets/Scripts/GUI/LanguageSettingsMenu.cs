using UnityEngine;
using System.Collections;

public class LanguageSettingsMenu : MonoBehaviour {

	
	public static void DisplayLanguageMenu() {

	int edgePaddingSide = MenuManager.edgePaddingSide;
	int edgePaddingTop = MenuManager.edgePaddingTop;
	int spaceBetweenButtons = MenuManager.spaceBetweenButtons;
	int newWidth = Screen.width - edgePaddingSide * 2;
	int newHeight = Screen.height - edgePaddingTop * 2;
	
	int centerPointButtonX = (newWidth-MenuManager.buttonsWidth)/2;

	//Make a group on the center of the screen
    GUI.BeginGroup(new Rect(0, MenuManager.groupY, newWidth, newHeight));
	
	if (GUI.Button (new Rect (centerPointButtonX, MenuManager.scaleHeight(30), MenuManager.buttonsWidth, MenuManager.buttonsHeight), LanguageDictionary.GetLanguageString("LanguageEnglish"))) {
		LanguageDictionary.selectedLanguage = 0;
		
	}
	
		if (GUI.Button (new Rect (centerPointButtonX, MenuManager.scaleHeight(30)+spaceBetweenButtons, MenuManager.buttonsWidth, MenuManager.buttonsHeight), LanguageDictionary.GetLanguageString("LanguageDanish"))) {
		LanguageDictionary.selectedLanguage = 1;
		
	}
		
	if (GUI.Button (new Rect (centerPointButtonX, MenuManager.scaleHeight(30)+spaceBetweenButtons*4f, MenuManager.buttonsWidth, MenuManager.buttonsHeight), LanguageDictionary.GetLanguageString("back"))) {
			MenuManager.menuBehaviour = GameSettingsMenu.DisplayGameSettingsMenu;
	}
	
	GUI.EndGroup ();
		
	}
}
