﻿using UnityEngine;
using System.Collections;

public class LivesGUI : MonoBehaviour {
	private Rect basePositionRect;
	public Texture2D icon;
	public GUIStyle iconStyle;
	private int lives;
	private TextMesh childText;
	private Animation lifeLostAnim;

	void Awake() {
		GameState.Instance.Life += new GameState.LifeHandler (OnLifeLost);
		this.basePositionRect = new Rect(Screen.width - 65, 70, 56, 56);
		this.lives = GameState.Instance.LivesLeft;
		this.childText = this.GetComponentInChildren<TextMesh>();
	}

	void OnLifeLost(int lives) {
		this.lives = lives;
		this.childText.text = this.lives.ToString();
	}

	void OnGUI() {
		GUI.DrawTexture(this.basePositionRect, this.icon);
	}
}