﻿using UnityEngine;
using System.Collections;

//Implementation of the Main Menu Screen
public class MenuManager : MonoBehaviour {

	public Texture frameImg;										//The texture for the background picture infront of the main scence
	public Texture gameTitle;										//The texture of the Game Title
	public Texture buttonNormal;
	public Texture buttonHover;
	public Texture buttonActive;
	public GUIStyle onBackgroundStyle;									//The style of the menu buttons
	public GUIStyle highScoreStyle;									//The style of the menu buttons
	public GUIStyle productionStyle;								//The style of the menu buttons
	public GUIStyle creditsStyle;

	public static int spaceBetweenButtons;						//The space between the menu buttons
	public static int buttonsWidth;							//The width of the buttons
	public static int buttonsHeight;							//The width of the buttons
    public static int groupY;                               // the starting point of the grouping on the y-axis
	public static bool muted = false;								//The starting state of our global sound
	private bool firstTime = false;

	public static int edgePaddingSide = scaleWidth(119);							//The space from the edge of 
	public static int edgePaddingTop = scaleHeight(145);							//The space from the edge of 

	public delegate void MenuBehaviour();
	public static MenuBehaviour menuBehaviour;



	void Start()
	{
		CreditsMenu.setStyle (onBackgroundStyle,creditsStyle);
		LeaderBoardMenu.setStyle (highScoreStyle);
		ResultsMenu.setStyle (onBackgroundStyle);
		MainMenu.setStyle (productionStyle);

		menuBehaviour = MainMenu.DisplayMainMenu;

		Fabric.EventManager.Instance.PostEvent ("PauseMusic", Fabric.EventAction.PlaySound);

		//Checks if the plyaer just came from a game and if so is send to the result screen
		if (PlayerPrefs.GetInt ("isResult") == 1) {
			menuBehaviour = ResultsMenu.DisplayResultsMenu;
			PlayerPrefs.DeleteKey ("isResult");
			//PlayerPrefs.DeleteKey ("timeLasted");
			//updates the HighScore
			HighScoreChecker.CheckForHighscore(PointSystem.TotalPoints);
		}
	}


	void OnGUI() {

		if (!firstTime) {
			//change the background of the soap
			GUI.skin.button.normal.background = (Texture2D)buttonNormal;
			GUI.skin.button.hover.background = (Texture2D)buttonHover;
			GUI.skin.button.active.background = (Texture2D)buttonActive;
			firstTime = true;
		}

		//Sets the button size
		buttonsWidth = scaleWidth(340);
		buttonsHeight = scaleHeight(150);
        spaceBetweenButtons = scaleHeight(110);
        groupY = scaleHeight(300);

		GUI.BeginGroup(new Rect(edgePaddingSide, edgePaddingTop, Screen.width-edgePaddingSide*2, Screen.height-edgePaddingTop*2));

		//Make a box to see the group on the Screen and asign it the texture passed from inspector
		GUI.DrawTexture (new Rect (0, 0, Screen.width-edgePaddingSide*2, Screen.height-edgePaddingTop*2), frameImg);

		// Calculate the title position by using a defined width and find the suitable height based on the texture
		int titlePaddingSide = scaleWidth (169-119);
		int titlePaddingTop = scaleHeight (175-145);
        int titleWidth = Screen.width - edgePaddingSide * 2 - titlePaddingSide * 2;
		int titleHeight = titleWidth * gameTitle.height/gameTitle.width;
		GUI.DrawTexture (new Rect (titlePaddingSide, titlePaddingTop,titleWidth, titleHeight), gameTitle);
			
		// Calls a delegate that shows the button on the screen
		if(menuBehaviour != null)
			menuBehaviour ();

		GUI.EndGroup ();	//End of the frame background image

	}

	// scales pixel from the target resolution 800X1232 to any screen resolution
	public static int scaleWidth(int originalWidth){
		return (int)((float)Screen.width/(800f/originalWidth));
	}

	public static int scaleHeight(int originalHeight){
		return (int)((float)Screen.height/(1232f/originalHeight));
	}
}
