﻿using UnityEngine;
using System.Collections;

public class InstructionsMenu : MonoBehaviour {

	public static void DisplayInstructionsMenu() {
		
		int edgePaddingSide = MenuManager.edgePaddingSide;
		int edgePaddingTop = MenuManager.edgePaddingTop;
		int spaceBetweenButtons = MenuManager.spaceBetweenButtons;
		int productionMenuHeight = 215;							//The height of the production menu box
		
		int newWidth = Screen.width - edgePaddingSide * 2;
		int newHeight = Screen.height - edgePaddingTop * 2;

	
		//Make a group for the buttons
        GUI.BeginGroup(new Rect(0, 0, newWidth, newHeight));


		//Finds the center point on the x-axis for the buttons
		int centerPointButtonX = (newWidth-MenuManager.buttonsWidth)/2;
	
		//Show picture
		if(GUI.Button(new Rect(0, MenuManager.scaleHeight(30)+spaceBetweenButtons*3, newWidth/*MenuManager.scaleWidth(500)*/, MenuManager.scaleHeight (1000)),(Texture)Resources.Load ("How_to_play_screenshot_Arrows_0003_gla"),new GUIStyle()))
		{
			MenuManager.menuBehaviour = MainMenu.DisplayMainMenu;
		}

		/*
		if (GUI.Button (new Rect (centerPointButtonX, MenuManager.scaleHeight(30)+spaceBetweenButtons*4, MenuManager.buttonsWidth, MenuManager.buttonsHeight),LanguageDictionary.GetLanguageString("back"))) {
			MenuManager.menuBehaviour = MainMenu.DisplayMainMenu;
		}
		*/

		GUI.EndGroup();
		

	}
}
