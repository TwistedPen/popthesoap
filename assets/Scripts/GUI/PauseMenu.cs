﻿using UnityEngine;
using System.Collections;

public class PauseMenu : MonoBehaviour
{
    public Texture logoTexture;
    public Texture bgTexture;
    public GUIStyle buttonStyle;

    bool drawPauseMenu = false;

    void Start()
    {
        GameState.Instance.Pause += new GameState.PauseHandler(DisplayPauseMenu);
    }

    static int buttonHeight = 196;
    static int buttonWidth = 296;

    //Restart
    //Back to main menu
    void DisplayPauseMenu(bool state)
    {
        drawPauseMenu = state;
    }

    static int edgePaddingSide = MenuManager.edgePaddingSide;
    static int edgePaddingTop = MenuManager.edgePaddingTop;
    static int spaceBetweenButtons = MenuManager.scaleHeight(110);
    static int buttonsWidth = MenuManager.scaleWidth(340);
    static int buttonsHeight = MenuManager.scaleHeight(150);

    static int newWidth = Screen.width - edgePaddingSide * 2;
    static int newHeight = Screen.height - edgePaddingTop * 2;

    static int centerPointButtonX = (newWidth - MenuManager.buttonsWidth) / 2;

    void OnGUI()
    {
        if(drawPauseMenu)
        {
            GUI.BeginGroup(new Rect(edgePaddingSide, edgePaddingTop, Screen.width - edgePaddingSide * 2, Screen.height - edgePaddingTop * 2));

            //Draw background
            GUI.DrawTexture(new Rect(0, 0, Screen.width - edgePaddingSide * 2, Screen.height - edgePaddingTop * 2), bgTexture);

            //Draw logo
            int titlePaddingSide = MenuManager.scaleWidth(169 - 119);
            int titlePaddingTop = MenuManager.scaleHeight(175 - 145);
            int titleWidth = Screen.width - edgePaddingSide * 2;
            int titleHeight = titleWidth * logoTexture.height / logoTexture.width;
            GUI.DrawTexture(new Rect(titlePaddingSide, titlePaddingTop, Screen.width - edgePaddingSide * 2 - titlePaddingSide * 2, titleHeight), logoTexture);

            if(GUI.Button(new Rect(centerPointButtonX, MenuManager.scaleHeight(200) + spaceBetweenButtons*2, buttonsWidth, buttonsHeight), LanguageDictionary.GetLanguageString("restart")))
            {
                Application.LoadLevel("DefaultScene");
            }

            if(GUI.Button(new Rect(centerPointButtonX, MenuManager.scaleHeight(200) + spaceBetweenButtons * 3, buttonsWidth, buttonsHeight), LanguageDictionary.GetLanguageString("backToMenu")))
            {
                Application.LoadLevel("Menus");
            }

            GUI.EndGroup();
        }
    }
}
