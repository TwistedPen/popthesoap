using UnityEngine;
using System.Collections;

public class InputHighScoreMenu : MonoBehaviour {

	public static string playerName = "";

	public static void DisplayInputHighScoreMenu() {

		int backgroundSize = MenuManager.edgePaddingSide;
		int spaceBetweenButtons = MenuManager.spaceBetweenButtons;
		int buttonsWidth = MenuManager.buttonsWidth;
		
		//Make a group for the buttons
		GUI.BeginGroup(new Rect(backgroundSize, Screen.height/2-backgroundSize, Screen.width-170, Screen.height-2*backgroundSize));

		GUI.Label (new Rect (5, 30, Screen.width-buttonsWidth, 40 ),LanguageDictionary.GetLanguageString("position") + ": " + HighScoreChecker.positionPointer);

		GUI.Label (new Rect (5, 30+spaceBetweenButtons, Screen.width-buttonsWidth, 40 ),LanguageDictionary.GetLanguageString("enterName"));

		playerName = GUI.TextField (new Rect (5, 30+spaceBetweenButtons*2, Screen.width-buttonsWidth, 30), playerName);

		if (GUI.Button (new Rect (5, 30+spaceBetweenButtons*3, Screen.width-buttonsWidth, 30),LanguageDictionary.GetLanguageString("submit"))) {

			HighScoreChecker.SortPlayersPrefs();
			HighScoreChecker.InsertHighScore(PointSystem.TotalPoints);
			MenuManager.menuBehaviour = ResultsMenu.DisplayResultsMenu;
						
		}
		
		if (GUI.Button (new Rect (5, 30+spaceBetweenButtons*4, Screen.width-buttonsWidth, 30),LanguageDictionary.GetLanguageString("proceed"))) {
			MenuManager.menuBehaviour = ResultsMenu.DisplayResultsMenu;
		}
		
		GUI.EndGroup();

	}
}
