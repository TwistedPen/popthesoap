using UnityEngine;
using System.Collections;

public class AudioSettingsMenu : MonoBehaviour {

	public static void DisplayAudioMenu() {
		
	int edgePaddingSide = MenuManager.edgePaddingSide;
	int edgePaddingTop = MenuManager.edgePaddingTop;
	int spaceBetweenButtons = MenuManager.spaceBetweenButtons;
	int newWidth = Screen.width - edgePaddingSide * 2;
	int newHeight = Screen.height - edgePaddingTop * 2;

	int centerPointButtonX = (newWidth-MenuManager.buttonsWidth)/2;
	
	//Make a group on the center of the screen
    GUI.BeginGroup(new Rect(0, MenuManager.groupY, newWidth, newHeight));
	
	if (MenuManager.muted == false)
	{
	if (GUI.Button (new Rect (centerPointButtonX, MenuManager.scaleHeight(30), MenuManager.buttonsWidth, MenuManager.buttonsHeight), LanguageDictionary.GetLanguageString("soundOff"))) {
			AudioListener.volume = 0.0F;
			MenuManager.muted=true;
		} 
	}
	if (MenuManager.muted) {
		if (GUI.Button (new Rect (centerPointButtonX, MenuManager.scaleHeight(30), MenuManager.buttonsWidth, MenuManager.buttonsHeight), LanguageDictionary.GetLanguageString("soundOn"))) {
			AudioListener.volume = 1.0F;
			MenuManager.muted=false;
		}
	}
	if (GUI.Button (new Rect (centerPointButtonX, MenuManager.scaleHeight(30)+spaceBetweenButtons*4, MenuManager.buttonsWidth, MenuManager.buttonsHeight), LanguageDictionary.GetLanguageString("back"))) {
			MenuManager.menuBehaviour = GameSettingsMenu.DisplayGameSettingsMenu;
	}
	
	GUI.EndGroup ();

	}
}
