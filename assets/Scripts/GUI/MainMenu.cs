﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour { 

	static GUIStyle productionTextStyle;
	
	public static void setStyle(GUIStyle style){
		productionTextStyle = style;
	}

	public static void DisplayMainMenu() {

		int edgePaddingSide = MenuManager.edgePaddingSide;
		int edgePaddingTop = MenuManager.edgePaddingTop;
		int spaceBetweenButtons = MenuManager.spaceBetweenButtons;
		int productionMenuHeight = 215;							//The height of the production menu box
	
		int newWidth = Screen.width - edgePaddingSide * 2;
		int newHeight = Screen.height - edgePaddingTop * 2;

		//Make a group for the buttons
		GUI.BeginGroup(new Rect(0, MenuManager.groupY, newWidth, newHeight));

		//Finds the center point on the x-axis for the buttons
		int centerPointButtonX = (newWidth-MenuManager.buttonsWidth)/2;

		if (GUI.Button (new Rect (centerPointButtonX, MenuManager.scaleHeight(30), MenuManager.buttonsWidth, MenuManager.buttonsHeight), LanguageDictionary.GetLanguageString("startGame"))) {
			Application.LoadLevel(1);										//Load the Starting Level
		}
		
		if (GUI.Button (new Rect (centerPointButtonX, MenuManager.scaleHeight(30)+spaceBetweenButtons, MenuManager.buttonsWidth, MenuManager.buttonsHeight), LanguageDictionary.GetLanguageString("instructions"))) {
			MenuManager.menuBehaviour = InstructionsMenu.DisplayInstructionsMenu;
		}
		
		if (GUI.Button (new Rect (centerPointButtonX, MenuManager.scaleHeight(30)+spaceBetweenButtons*2, MenuManager.buttonsWidth, MenuManager.buttonsHeight), LanguageDictionary.GetLanguageString("gameSettings"))) {
			MenuManager.menuBehaviour = GameSettingsMenu.DisplayGameSettingsMenu;	
		}
		
		if (GUI.Button (new Rect (centerPointButtonX, MenuManager.scaleHeight(30)+spaceBetweenButtons*3, MenuManager.buttonsWidth, MenuManager.buttonsHeight),LanguageDictionary.GetLanguageString("leaderBoard"))) {
			MenuManager.menuBehaviour = LeaderBoardMenu.DisplayLeaderBoardMenu;
		}
		
		if (GUI.Button (new Rect (centerPointButtonX, MenuManager.scaleHeight(30)+spaceBetweenButtons*4, MenuManager.buttonsWidth, MenuManager.buttonsHeight),LanguageDictionary.GetLanguageString("credits"))) {
			MenuManager.menuBehaviour = CreditsMenu.DisplayCreditsMenu;
		}

		//DADIU text
		//productionStyle.fontSize = 8;
		//Make a group on the center of the screen
		GUI.Label (new Rect (centerPointButtonX-5, MenuManager.scaleHeight(65)+spaceBetweenButtons*5, MenuManager.buttonsWidth, productionMenuHeight),LanguageDictionary.GetLanguageString("productionOf"),productionTextStyle);

		GUI.EndGroup();

	}
	
}
