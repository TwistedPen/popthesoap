﻿/*
 * Author: Georgi Muerov
 */
using UnityEngine;
using System.Collections;

/// <summary>
/// A component used to control the rotation of the soap.
/// </summary>

public class SoapRotation : MonoBehaviour {

	public Vector3 rotationVector;
	public float rotationSpeed;

    // Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if (!GameState.Instance.IsPaused)
			transform.Rotate(rotationVector * rotationSpeed);
	}

}
