﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoapExplosionInfo
{
    /// <summary>
    /// Total number of soaps that actually exploded (contributed to the chain reaction).
    /// </summary>
    public int totalExplodedSoaps = 0;
    /// <summary>
    /// Total number of soaps that were destroyed (didn't contribute to the chain reaction).
    /// </summary>
    public int totalDestroyedSoaps = 0;
    /// <summary>
    /// Tracks the number of currently exploding soaps. Should be 0 after the explosion chain ends.
    /// </summary>
    public int explosionCounter = 0;

    public Vector3 firstSoapPosition;

    public List<int> soapsDestroyedByEachExplosion = new List<int>();
    public List<int> soapsExplodedInEachStage = new List<int>();
    public Dictionary<SoapType.Type, int> soapsDestroyedByType = new Dictionary<SoapType.Type, int>();

    public SoapExplosionInfo()
    {
        this.soapsDestroyedByType[SoapType.Type.Normal] = 0;
        this.soapsDestroyedByType[SoapType.Type.Red] = 0;
        this.soapsDestroyedByType[SoapType.Type.Green] = 0;
    }

    override public string ToString()
    {
        string result = totalDestroyedSoaps.ToString() + " / " + totalExplodedSoaps.ToString() + " |";

        foreach(var soaps in this.soapsDestroyedByEachExplosion)
        {
            result += " " + soaps;
        }

        result += " |";

        foreach(var soaps in this.soapsExplodedInEachStage)
        {
            result += " " + soaps;
        }

        result += " ||";

        foreach(var keyValue in this.soapsDestroyedByEachExplosion)
        {
            result += " " + keyValue.ToString();
        }

        return result;
    }
}
