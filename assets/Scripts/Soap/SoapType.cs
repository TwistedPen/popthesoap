﻿using UnityEngine;
using System.Collections;

public class SoapType : MonoBehaviour
{
    public enum Type 
    {
        Normal,
        Red,
        Green
    }

    public Type type;

    // 0 = Complete stand-still. 100 = Normal gravity.
    public float fallspeed;
	// 0 = Complete stand-still. 100 = Normal gravity.
	public float fallspeedSmall;
    // Fallspeed on descent = this.fallspeed +- randomFallspeedChange
    public float randomFallspeedChange;

    void Start()
    {
        float minFall = this.fallspeed * (1f - this.randomFallspeedChange);
        float maxFall = this.fallspeed * (1f + this.randomFallspeedChange);
        this.fallspeed = UnityEngine.Random.Range(minFall, maxFall);
		minFall = this.fallspeedSmall * (1f - this.randomFallspeedChange);
		maxFall = this.fallspeedSmall * (1f + this.randomFallspeedChange);
		this.fallspeedSmall = UnityEngine.Random.Range(minFall, maxFall);
    }
}