﻿/*
 * Author: Wojciech Terepeta
 */
using UnityEngine;
using System.Collections;

public class SoapDeflect : MonoBehaviour
{
    public float xThreshold = 0.1f;
    public float minX = 1f;
    public float maxX = 2f;

    void OnCollisionStay(Collision collision)
    {
        if(Mathf.Abs(collision.relativeVelocity.x) < this.xThreshold)
        {
            float x = Random.Range(this.minX, this.maxX);
            collision.rigidbody.velocity = new Vector3(x, collision.rigidbody.velocity.y, 0f);
        }
    }
}
