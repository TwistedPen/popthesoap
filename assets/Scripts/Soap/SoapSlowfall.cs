﻿using UnityEngine;
using System.Collections;

//Currently should be applied to soap prefab
public class SoapSlowfall : MonoBehaviour
{
    private bool isActive;
    private float baseSlowdownSmall = 0f;
    private float baseSlowdownOther = 0f;
    private DifficultyManager difficultyManager;
    private SoapDecay soapDecay;

    void Awake()
    {
        var soapType = this.gameObject.GetComponent<SoapType>();
        this.baseSlowdownSmall = soapType.fallspeedSmall;
        this.baseSlowdownOther = soapType.fallspeed;
        this.difficultyManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<DifficultyManager>();
    }

    void Start()
    {
        this.soapDecay = this.gameObject.GetComponent<SoapDecay>();
    }

    public void SetActive(bool active)
    {
        this.isActive = active;
    }

    void Update()
    {
        if(isActive)
        {
            float baseSlowdown = 0f;
            int difficulty = this.difficultyManager.GetCurrentDifficulty();
            if(this.soapDecay.GetState() == SoapDecay.State.Small)
            {
                baseSlowdown = this.baseSlowdownSmall * this.difficultyManager.GetFallspeedSmall(difficulty);
            }
            else
            {
                baseSlowdown = this.baseSlowdownOther * this.difficultyManager.GetFallspeed(difficulty);
            }

            //Debug.Log(baseSlowdown);
            float baseFall = Mathf.Max(-9.81f * (baseSlowdown / 100f), -9.81f);

            rigidbody.velocity = new Vector3(
                rigidbody.velocity.x,
                Mathf.Max(baseFall, rigidbody.velocity.y),
                rigidbody.velocity.z);
        }
    }
}