﻿using UnityEngine;
using System.Collections;

public class ThrownRecently : MonoBehaviour
{
    private bool thrownRecently = false;

    public bool IsTrue()
    {
        return this.thrownRecently;
    }

    public void SetThrown()
    {
        this.thrownRecently = true;
    }

    void OnTriggerExit(Collider col)
    {
        thrownRecently = false;
    }
}
