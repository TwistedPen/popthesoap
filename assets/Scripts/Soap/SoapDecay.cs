﻿/*
 * Author: Thais Thomas Borgholm, Georgi Muerov, Wojciech Terepeta
 */
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoapDecay : MonoBehaviour
{
    /// <summary>
    /// The number of different explosion chains currently in progress.
    /// </summary>
    private static int explosionsInProgress = 0;
    /// <summary>
    /// To remember what those values were before kicking in the time dilation.
    /// </summary>
    private static float timeScale = 0, fixedDeltaTime = 0;

    public enum TypeOfDecay
    {
        Time,
        Click
    }

    public enum State
    {
        Big = 0,
        Medium,
        Small, // Ready to be detonated
        Explode
    }

    public bool shrinkToDeath = false;
    public bool explodeAsCollateral = true;
    public int clicksBeforeShrink = 3;
    public float secondsBeforeShrink = 2.0f;
    public TypeOfDecay typeOfDecay = TypeOfDecay.Time;
    public float explosionRadius = 1.5f;
    public float explosionDelay = 0.2f;
    public float timeDilationFactor = 0.1f;
    public bool allowExplosionInGreenArea = false;
    public bool showDebugOutput = false;
    public GameObject explosionPrefab;
    public bool showBlueExplosion = true;
    public float smallSoapZAxis = 0.2F;

    public bool Explodable
    {
        get;
        private set;
    }

    private int currentClicks = 0;
    private float currentTime;
    private State state = 0;

	public State GetState() {
		return this.state;
	}

    void Start()
    {
        this.Explodable = false;

        if(this.typeOfDecay == TypeOfDecay.Time)
        {
            this.currentTime = secondsBeforeShrink;
        }
    }

    void Update()
    {
        if(this.typeOfDecay == TypeOfDecay.Time)
        {
            this.currentTime -= Time.deltaTime;
            if(this.currentTime <= 0)
            {
                this.currentTime = this.secondsBeforeShrink;
                ChangeSoapSize(this.state + 1);
            }
        }
    }

    public IEnumerator OnClicked()
    {
        bool inGreenArea = GreenAreaChecker.Instance.ObjectInArea(this.gameObject);
        bool colliderInGreenArea = GreenAreaChecker.Instance.ObjectInArea(this.gameObject, GreenAreaChecker.DetectionMethod.SphereCollider);

        // Make the soap explode if:
        // it can actually explode + its mesh and collider aren't in the green area (unless it's allowed to explode in the green area)
        if(this.Explodable && ((!inGreenArea && !colliderInGreenArea) || this.allowExplosionInGreenArea) && this.state != State.Explode)
        {
            SoapExplosionInfo explosionInfo = new SoapExplosionInfo();
            explosionInfo.firstSoapPosition = this.transform.position;
            yield return StartCoroutine(this.Explode(explosionInfo));
            GameState.Instance.OnSoapChainExplosion(explosionInfo);
        }
        else if(inGreenArea && ++this.currentClicks >= clicksBeforeShrink)
        {
            currentClicks = 0;

            if(this.state == State.Big || this.state == State.Medium || this.shrinkToDeath)
            {
                ChangeSoapSize(this.state + 1);
            }
        }
    }

    //changes the shrinkState and the size of the soap
    void ChangeSoapSize(State state)
    {
        this.state = state;

        //Change the actual size/model of the soap
        switch(state)
        {
            case State.Big:
                this.gameObject.GetComponent<MeshFilter>().mesh = LoadManager.Instance.BigSoapMesh;;
                //this.gameObject.GetComponent<MeshFilter>().mesh =
                //    Resources.Load<Mesh>("soap_big_v0002_EXP_mlh");
                GameState.Instance.OnDecayStatusChange(0, this.gameObject);
                break;

            case State.Medium:
                this.gameObject.GetComponent<MeshFilter>().mesh = LoadManager.Instance.MediumSoapMesh;;
                //this.gameObject.GetComponent<MeshFilter>().mesh =
                //    Resources.Load<Mesh>("soap_medium_v0002_EXP_mlh");
                GameState.Instance.OnDecayStatusChange(1, this.gameObject);
                break;

            case State.Small:
                this.Explodable = true;
                this.gameObject.GetComponent<MeshFilter>().mesh = LoadManager.Instance.SmallSoapMesh;;
                //this.gameObject.GetComponent<MeshFilter>().mesh =
                //    Resources.Load<Mesh>("soap_small_v0002_EXP_mlh");
                GameState.Instance.OnDecayStatusChange(2, this.gameObject);
                this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z - smallSoapZAxis);
                break;

            // For the soaps that can shrink into death.
            case State.Explode:
                SoapExplosionInfo info = new SoapExplosionInfo();
                info.totalDestroyedSoaps = 1;
                GameState.Instance.OnSoapChainExplosion(info);
                GameObject.Destroy(this.gameObject);
                break;

            default:
                Debug.LogWarning("Soap is trying to change size to an invalid one!");
                break;
        }

    }

    /// <summary>
    /// Sets the soap to explode/be destroyed, depending on the soaps state.
    /// </summary>
    /// <param name="explosionInfo">Contains the information about the explosion.</param>
    /// <param name="explosionStage">The distance (in number of explosions) from the 1st explosion in the chain.</param>
    private IEnumerator Explode(SoapExplosionInfo explosionInfo, int explosionStage = 0)
    {
        // Ignore if the soap is already set to explode/be destroyed.
        if(this.state == State.Explode)
        {
            if(this.showDebugOutput)
            {
                Debug.Log("Ignoring a soap that's already exploding/being destroyed.");
            }
            yield break;
        }

        // If this soap doesn't blow up when exploded, but just reduces in size...
        // This is triggered by being Explode'd by another soap, when your state isn't
        // even small yet. In that case, become as low as small; if you were already small,
        // then exploding is fine.
        if(this.explodeAsCollateral == false && this.state < State.Small)
        {
            this.ChangeSoapSize(this.state + 1);
            yield break;
        }

        this.state = State.Explode;
        this.renderer.enabled = false;
        explosionInfo.explosionCounter += 1;

        // If this isn't a smallest soap, then just add info about it and destroy it.
        if(!this.Explodable)
        {
            explosionInfo.totalDestroyedSoaps += 1;
            explosionInfo.soapsDestroyedByEachExplosion[explosionStage] += 1;
            explosionInfo.explosionCounter -= 1;
            explosionInfo.soapsDestroyedByType[this.GetComponent<SoapType>().type] += 1;

            GameState.Instance.OnSoapDestroyed(this.transform.position);

            Destroy(this.gameObject);
            yield break;
        }

        SoapDecay.explosionsInProgress += 1;
        explosionInfo.totalExplodedSoaps += 1;
        explosionInfo.soapsDestroyedByType[this.GetComponent<SoapType>().type] += 1;

        // Add a new explosion stage to the statistics (destroyed soaps)
        if(explosionInfo.soapsDestroyedByEachExplosion.Count <= explosionStage)
        {
            explosionInfo.soapsDestroyedByEachExplosion.Add(0);
        }

        // Add a new explosion stage to the statistics (exploded soaps) or increment the counter
        if(explosionInfo.soapsExplodedInEachStage.Count <= explosionStage)
        {
            explosionInfo.soapsExplodedInEachStage.Add(1);
        }
        else
        {
            explosionInfo.soapsExplodedInEachStage[explosionStage] += 1;
        }

        

        // Let only the initial explosion to start the time dilation
        if(explosionStage > 0)
        {
            // Delay the non-initial explosions
            yield return new WaitForSeconds(TimeUtility.GetActualTime(this.explosionDelay));
        }
        else if(SoapDecay.explosionsInProgress <= 1)
        {
            // Set time dilation, if this is the initial explosion and there aren't any other explosions already in progress.
            TimeUtility.SetTimeDilationFactor(this.timeDilationFactor, out SoapDecay.timeScale, out SoapDecay.fixedDeltaTime);
        }

        // Get the soaps in radius and destroy them, triggering chain reaction
        var collidersInRadius = Physics.OverlapSphere(this.transform.position, this.explosionRadius, 1 << this.gameObject.layer);
        // The soaps that create a chain reaction
        var soapListExploding = new LinkedList<SoapDecay>();
        // The soaps that are just disappear
        var soapListDestroyed = new LinkedList<SoapDecay>();

        // Find soaps in radius that will be destroyed/explode
        foreach(var collider in collidersInRadius)
        {
            SoapDecay soap = collider.GetComponent<SoapDecay>();

            // Ignore colliders with no SoapDecay component (there shouldn't be any).
            if(soap == null)
            {
                if(this.showDebugOutput)
                {
                    Debug.LogWarning("Collider in explosion radius was not a soap and was detected!");
                }
                continue;
            }

            // Ignore the current soap.
            if(soap == this)
            {
                if(this.showDebugOutput)
                {
                    Debug.Log("Ignoring the current soap.");
                }
                continue;
            }

            // Add the soap to the appropriate list.
            if(soap.Explodable)
            {
                soapListExploding.AddLast(soap);
            }
            else
            {
                soapListDestroyed.AddLast(soap);
            }
        }

        if(this.showDebugOutput)
        {
            Debug.Log("Exploding soap! Coordinates: " + this.transform.position.ToString() + ", radius: " + this.explosionRadius);
            Debug.Log("Other soaps in radius: " + (soapListExploding.Count + soapListDestroyed.Count).ToString() + " (from " + collidersInRadius.Length + " colliders)");
            Debug.Log(soapListExploding.Count.ToString() + " soaps are ready to explode");
        }

        SpawnExplosion(soapListDestroyed);

        // Destroy/explode the found soaps
        for(int i = soapListDestroyed.Count; i > 0; --i)
        {
            SoapDecay soap = soapListDestroyed.Last.Value;
            soapListDestroyed.RemoveLast();
            StartCoroutine(soap.Explode(explosionInfo, explosionStage));
        }

        for(int i = soapListExploding.Count; i > 0; --i)
        {
            SoapDecay soap = soapListExploding.Last.Value;
            soapListExploding.RemoveLast();
            StartCoroutine(soap.Explode(explosionInfo, explosionStage + 1));
        }

        // Count the soaps exploded until this stage
        int soapsUpTillNow = 0;
        for(int stage = 0; stage <= explosionStage; ++stage)
        {
            soapsUpTillNow += explosionInfo.soapsExplodedInEachStage[stage];
        }


        do
        {
            // Wait for the other explosions to finish.
            yield return new WaitForSeconds(TimeUtility.GetActualTime(this.explosionDelay));
        }
        while(explosionInfo.explosionCounter > soapsUpTillNow);

        // Revert the time dilation effect, if this is the initial explosion and there are no other explosions in progress.
        if(explosionStage <= 0 && SoapDecay.explosionsInProgress <= 1)
        {
            TimeUtility.SetTimeScale(SoapDecay.timeScale, SoapDecay.fixedDeltaTime);
        }

        SoapDecay.explosionsInProgress -= 1;
        // Don't delay destroy by calling Destroy(GameObject, float), otherwise it'll only get points from the very first explosion.
        explosionInfo.explosionCounter -= 1;
        Destroy(this.gameObject);

        if(this.showDebugOutput)
        {
            Debug.Log("Explosion: " + explosionInfo.ToString());
        }
    }

	private void SpawnExplosion(LinkedList<SoapDecay> destroyList)
	{
		float diameter = 2 * this.explosionRadius;
		
		if(showBlueExplosion)
		{
			GameObject explosion = Instantiate(this.explosionPrefab, this.transform.position, Quaternion.identity) as GameObject;
			
			explosion.transform.Rotate(90f, 0f, 0f);
			explosion.transform.localScale = new Vector3(diameter, explosion.transform.localScale.y, diameter);
		}
		GameState.Instance.OnSoapSingleExplosion(this.transform.position, diameter, destroyList);
	}
}
