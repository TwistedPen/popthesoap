﻿/*
 * Author: Wojciech Terepeta, Thais Thomas Borgholm, Ulf Simonsen
 */
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FeedbackSystem : Singleton<FeedbackSystem>
{
    public bool debugEnabled = true;
    public GameObject comboTextPrefab;
    public float keepComboTime = 1;
    public float keepScoreTime;
    public GameObject scoreTextPrefab;
    public float blinkTime = 0.5F;
    public int scoreFontSize = 70;
    public float scoreXAxis;
    public float scoreYAxis;
    public float rColor;
    public float gColor;
    public float bColor;
    private Vector3 setPosition;



    void Start()
    {
        var soapSpawners = FindObjectsOfType<PrefabSpawner>();

        foreach(var soapSpawner in soapSpawners)
        {
            soapSpawner.PrefabSpawn += new PrefabSpawner.Spawned(OnSoapSpawned);
        }

        GameState.Instance.GameOver += new GameState.GameOverHandler(OnGameOver);
        GameState.Instance.SoapChainExplosion += new GameState.SoapChainExplosionHandler(OnSoapChainExplosion);
        GameState.Instance.SoapSingleExplosion += new GameState.SoapSingleExplosionHandler(OnSoapSingleExplosion);
        GameState.Instance.SoapDestroyed += new GameState.SoapDestroyedHandler(OnSoapDestroyed);
        GameState.Instance.DecayStatus += new GameState.DecayStatusHandler(OnDecayStatusChange);
        GameState.Instance.DestructibleClicked += new GameState.DestructibleClickedHandler(OnDestructibleClicked);
        PointSystem.Instance.PointThresholdExceeded += new PointSystem.PointThresholdExceededHandler(OnPointThresholdExceeded);
        PointSystem.Instance.PointsChanged += new PointSystem.PointsChangedHandler(OnPointsChanged);
    }

    void OnDestructibleClicked(GameObject destructible)
    {
        if(GreenAreaChecker.Instance.ObjectInArea(destructible))
        {
            destructible.GetComponent<SoapClickParticles>().ClickGreenParticleStart(destructible);
        }
        else
        {
            destructible.GetComponent<SoapClickParticles>().ClickOutsideParticleStart(destructible);
        }

    }

    void OnPointsChanged(int newPoints, Vector3? firstExplosionPosition)
    {
        if(firstExplosionPosition != null)
        {
            GameObject scoreText = (GameObject) Instantiate(scoreTextPrefab);
            setPosition = (Vector3) firstExplosionPosition;
            setPosition.x += scoreXAxis;
            setPosition.y += scoreYAxis;
            scoreText.transform.position = setPosition;
            TimedDeath td = scoreText.AddComponent<TimedDeath>();
            td.timeBeforeDeath = keepScoreTime;
            scoreText.GetComponent<TextMesh>().text = newPoints.ToString();
            StartCoroutine(AnimateText(scoreText));
        }
    }    void OnGameOver()
    {

    }

    void OnPointThresholdExceeded(int thresholdIndex, ParticleSystem parSystem)
    {
        ParticleSystem particles = (ParticleSystem) Instantiate(parSystem);
        particles.transform.position = GameObject.Find("ParticleSpawner").transform.position;
    }

    void OnSoapChainExplosion(SoapExplosionInfo explosionInfo)
    {

    }

    void OnSoapSingleExplosion(Vector3 position, float diameter, LinkedList<SoapDecay> destroyedList)
    {
        this.gameObject.GetComponent<SoapExplodeParticles>().ExplotionParticleStart(position, diameter);
        this.gameObject.GetComponent<SoapExplodeParticles>().DestroyedSoapInExplotionParticleStart(destroyedList);
    }

    void OnSoapDestroyed(Vector3 position)
    {

    }

    void OnSoapSpawned()
    {

    }

    void OnDecayStatusChange(int status, GameObject obj)
    {
        //Make particle feedback on Soap decay change
        obj.GetComponent<SoapDecayParticles>().DecayParticleStart(status, obj);

        //Start particle bubble trail if small soap
        if(status == 2)
        {
            obj.GetComponent<BubbleTrailParticles>().StartBubbles(obj);
        }
    }
    void PrintComboText(string text)
    {
        GameObject comboText = (GameObject) Instantiate(comboTextPrefab);
        comboText.transform.position = new Vector3(-13F,12.5F,11.7F);
        
        TimedDeath td = comboText.AddComponent<TimedDeath>();
        td.timeBeforeDeath = keepComboTime;
        comboText.GetComponent<TextMesh>().text = text;
        if(debugEnabled)
        {
            
        }
    }
    private IEnumerator AnimateText(GameObject animee)
    {
        Color32 newColor = new Color32(107,209,235,180);
        Color32 anotherNewColor = new Color32(229,160,235,180);

        animee.GetComponent<TextMesh>().fontSize = scoreFontSize;
        animee.GetComponent<TextMesh>().color = newColor;
        yield return new WaitForSeconds(blinkTime);
        animee.GetComponent<TextMesh>().color = anotherNewColor;
        yield return new WaitForSeconds(blinkTime);
        animee.GetComponent<TextMesh>().color = newColor;
        yield return new WaitForSeconds(blinkTime);
        animee.GetComponent<TextMesh>().color = anotherNewColor;
    }
}
