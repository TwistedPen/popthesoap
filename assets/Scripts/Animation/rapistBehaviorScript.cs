﻿using UnityEngine;
using System.Collections;

public class rapistBehaviorScript : MonoBehaviour {

    private PrefabSpawner spawner;
	private Animator anim;
//	private float waitTime = 0.1f;

	// Use this for initialization
	void Start () 
	{
	}

	void Awake()
	{
        
        anim = GetComponent<Animator> ();
        this.spawner = GameObject.FindGameObjectWithTag("Spawner").GetComponent<PrefabSpawner>();
        this.spawner.PrefabSpawn += BeginThrowEvent;
		//type in event it needs to listen to - to know when to initialize a throw
	}


	void BeginThrowEvent() // method called when throw-event triggered
	{
		//yield return new WaitForSeconds(0.1f);
		anim.SetTrigger ("beginThrow");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
