﻿using UnityEngine;
using System.Collections;

public class LoadManager : Singleton<LoadManager>
{
    public Mesh BigSoapMesh
    {
        get;
        private set;
    }

    public Mesh MediumSoapMesh
    {
        get;
        private set;
    }
    public Mesh SmallSoapMesh
    {
        get;
        private set;
    }

    void Awake()
    {
        BigSoapMesh = Resources.Load<Mesh>("soap_big_v0002_EXP_mlh");
        MediumSoapMesh = Resources.Load<Mesh>("soap_medium_v0002_EXP_mlh");
        SmallSoapMesh = Resources.Load<Mesh>("soap_small_v0002_EXP_mlh");
    }
}
