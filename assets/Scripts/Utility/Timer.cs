﻿/*
 * Author: Martin Grunbaum, Wojciech Terepeta
 */
using UnityEngine;
using System.Collections;

/// <summary>
/// A timer component, which relies on Time.deltaTime.
/// </summary>
public class Timer : MonoBehaviour
{
    private float timeTaken = 0f;
    private bool isStarted = false;

    /// <summary>
    /// Yields the current elapsed time.
    /// </summary>
    /// <returns>The current elapsed time, 0 if the timer is stopped.</returns>
    public float GetTime()
    {
        return timeTaken;
    }

    /// <summary>
    /// Starts the timer.
    /// </summary>
    public void StartTimer()
    {
        this.isStarted = true;
    }

    /// <summary>
    /// Stops the timer and resets the elapsed time.
    /// </summary>
    /// <returns>The time elapsed before the timer was stopped.</returns>
    public float StopTimer()
    {
        this.isStarted = false;
        float timeT = this.timeTaken;
        this.timeTaken = 0f;

        return timeT;
    }

    /// <summary>
    /// Restarts the timer.
    /// </summary>
    /// <returns>The time elapsed before the timer was restarted.</returns>
    public float RestartTimer()
    {
        float time = this.StopTimer();
        this.StartTimer();

        return time;
    }

    void Update()
    {
        if(this.isStarted)
        {
            // This will 
            this.timeTaken += Time.deltaTime;
        }
    }
}