﻿/*
 * Author: Martin Grunbaum, Wojciech Terepeta, Ulf Simonsen, Georgi Muerov
 */
using UnityEngine;
using System.Collections;

public class DestructibleClicked : MonoBehaviour
{
    /// <summary>
    /// Give points on the first click or not.
    /// </summary>
    public bool givePoints = true;
    SoapDecay soapDecay;

    void Start()
    {
        this.soapDecay = this.GetComponent<SoapDecay>();
    }

    void OnMouseDown()
    {
        if(!GameState.Instance.IsPaused || !GameState.Instance.tutorialPassed)
        {
            GameState.Instance.OnDestructibleClicked(this.gameObject);
            StartCoroutine(this.soapDecay.OnClicked());
            this.givePoints = true;
        }
    } 
}
