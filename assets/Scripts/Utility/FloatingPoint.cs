﻿/*
 * Author: Wojciech Terepeta
 */
using UnityEngine;
using System.Collections;

public static class FloatingPoint
{
    public static readonly float epsilon = 0.0001f;


    // Courtesy of: http://stackoverflow.com/questions/3874627/floating-point-comparison-functions-for-c-sharp
    /// <summary>
    /// Checks if two floats are nearly equal to each other.
    /// </summary>
    /// <param name="a">The first value.</param>
    /// <param name="b">The second value.</param>
    /// <param name="epsilon"></param>
    /// <returns>True if the two floats are nearly equal, false otherwise.</returns>
    public static bool NearlyEqual(float a, float b, float epsilon)
    {
        float absA = Mathf.Abs(a);
        float absB = Mathf.Abs(b);
        float diff = Mathf.Abs(a - b);

        if(a == b)
        { 
            // shortcut, handles infinities
            return true;
        }
        else if(a == 0 || b == 0 || diff < float.MinValue)
        {
            // a or b is zero or both are extremely close to it
            // relative error is less meaningful here
            return diff < (epsilon * float.MinValue);
        }
        else
        { 
            // use relative error
            return diff / (absA + absB) < epsilon;
        }
    }
}
