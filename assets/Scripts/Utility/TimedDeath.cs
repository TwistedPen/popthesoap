﻿/*
 * Author: Wojciech Terepeta
 */
using UnityEngine;
using System.Collections;

public class TimedDeath : MonoBehaviour
{
    public float timeBeforeDeath = 0.5f;

    private float elapsedTime = 0f;

    void Update()
    {
        elapsedTime += Time.deltaTime;

        if(elapsedTime >= timeBeforeDeath)
        {
            Destroy(this.gameObject);
        }
    }
}
