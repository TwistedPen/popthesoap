﻿/*
 * Author: Martin Grunbaum, Wojciech Terepeta, Georgi Muerov
 */
using UnityEngine;
using System.Collections;

/// <summary>
/// A component which allows the parent GameObject to be destroyed on a collision 
/// (with any object or one with a specific tag).
/// </summary>
public class Destructible : MonoBehaviour
{
    /// <summary>
    /// This object will be destroyed upon collision with objects with this tag. 
    /// If the tag is empty, all collisions will result in death.
    /// </summary>
    public string destructibleTag;

    /// <summary>
    /// Occurs when prefab is destroyed.
    /// </summary>
    public delegate void PrefabDestroyedHandler();
    public event PrefabDestroyedHandler PrefabDestroyed;

    /// <summary>
    /// If destructibleTag is set, colliding will only destroy the
    /// GameObject Destructible is attached to if the object we collide
    /// with has that tag. If no destructibleTag is set, any collision
    /// will destroy this GameObject.
    /// </summary>
    void OnCollisionEnter(Collision collision)
    {
        if(this.destructibleTag.Length <= 0 ||
           this.destructibleTag.Equals(collision.gameObject.tag))
        {
            this.OnPrefabDestroyed();
			GameState.Instance.OnSoapLost();
			Destroy(this.gameObject);
		}
	}

    void OnPrefabDestroyed()
    {
        if(PrefabDestroyed != null)
            PrefabDestroyed();
    }
}
