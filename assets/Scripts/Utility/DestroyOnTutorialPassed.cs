﻿/*
 * Author: Wojciech Terepeta
 */
using UnityEngine;
using System.Collections;

public class DestroyOnTutorialPassed : MonoBehaviour
{
    void Update()
    {
        if(GameState.Instance.tutorialPassed)
        {
            Destroy(this.gameObject);
        }
    }
}
