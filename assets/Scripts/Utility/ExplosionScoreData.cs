﻿/*
 * Author: Georgi Muerov
 */

using UnityEngine;

/// <summary>
/// Explosion score data.
/// 
/// </summary>

public struct ExplosionScoreData{
    /// <summary>
    /// The score of the explosion.
    /// </summary>
    public int score;

    /// <summary>
    /// First time the explosion has been registered.
    /// </summary>
    public float initialTime;

    /// <summary>
    /// The position where the score should be displayed.
    /// </summary>
    public Vector2 position;
}