﻿/*
 * Author: Wojciech Terepeta
 */
using UnityEngine;
using System.Collections;

public static class TimeUtility
{
    public static readonly float defaultTimeScale;
    public static readonly float defaultFixedDeltaTime;
    public static bool showDebugOutput = false;

    // Values to be able to restore the game to the unpaused state.
    static float previousTimeScale;
    static float previousFixedDeltaTime;

    static TimeUtility()
    {
        TimeUtility.defaultTimeScale = Time.timeScale;
        TimeUtility.defaultFixedDeltaTime = Time.fixedDeltaTime;

        if(showDebugOutput)
        {
            Debug.Log("TimeUtility defaults: " + defaultTimeScale + ", " + defaultFixedDeltaTime);
        }
    }

    /// <summary>
    /// Yields the actual time taking <code>Time.timeScale</code> into account, e.g. when trying to delay something for a period of time.
    /// </summary>
    /// <param name="desiredTime">The desired time period.</param>
    /// <returns>The actual time value.</returns>
    public static float GetActualTime(float desiredTime)
    {
        return desiredTime * Time.timeScale;
    }

    /// <summary>
    /// Multiplies <code>Time.timeScale</code> and <code>Time.fixedDeltaTime</code> by the given factor. Will automatically pause if the factor is 0.
    /// </summary>
    /// <param name="factor">The time dilation factor.</param>
    /// <param name="timeScale">The value of <code>Time.timeScale</code> before the change.</param>
    /// <param name="fixedDeltaTime">The value of <code>Time.fixedDeltaTime</code> before the change.</param>
    public static void SetTimeDilationFactor(float factor, out float timeScale, out float fixedDeltaTime)
    {
        timeScale = Time.timeScale;
        fixedDeltaTime = Time.fixedDeltaTime;
        SetTimeScale(Time.timeScale * factor, Time.fixedDeltaTime * factor);

        if(showDebugOutput)
        {
            Debug.Log("TimeUtility set time dilation to: " + factor);
        }
    }

    /// <summary>
    /// Restores the time scale and fixed delta time to their previous values.
    /// </summary>
    public static void RestoreTimeScale()
    {
        Time.fixedDeltaTime = previousFixedDeltaTime;
        Time.timeScale = previousTimeScale;

        GameState.Instance.UpdatePause();
    }

    /// <summary>
    /// Resets the time scale and fixed delta time to their default values.
    /// </summary>
    public static void ResetTimeScale()
    {
        Time.fixedDeltaTime = defaultFixedDeltaTime;
        Time.timeScale = defaultTimeScale;

        GameState.Instance.UpdatePause();
    }

    /// <summary>
    /// Sets the time scale and fixed delta time in the game engine to facilitate pausing/resuming.
    /// </summary>
    /// <param name="timeScale">The new value for <code>Time.timeScale</code>.</param>
    /// <param name="fixedDeltaTime">The new value for <code>Time.fixedDeltaTime</code>.</param>
    public static void SetTimeScale(float timeScale, float fixedDeltaTime)
    {
        previousFixedDeltaTime = Time.fixedDeltaTime;
        previousTimeScale = Time.timeScale;

        Time.fixedDeltaTime = fixedDeltaTime;
        Time.timeScale = timeScale;

        GameState.Instance.UpdatePause();

        if(showDebugOutput)
        {
            Debug.Log("TimeUtility set time scale: " + timeScale + ", " + fixedDeltaTime);
        }
    }
}
