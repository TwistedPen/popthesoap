﻿using UnityEngine;
using System.Collections;

public class SoapClickParticles : MonoBehaviour {

	public ParticleSystem particleClickSoapGreen;
	public ParticleSystem particleClickSoapOutside;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	//Starts the Particle System on the Soap position for the soaps in greenarea
	public void ClickGreenParticleStart (GameObject obj)
	{
		ParticleSystem partSystem = (ParticleSystem)Instantiate (particleClickSoapGreen);
			partSystem.transform.position = obj.transform.position;
	}

	//Starts the Particle System on the Soap position for the soaps outside greenarea
	public void ClickOutsideParticleStart (GameObject obj)
	{
		ParticleSystem partSystem = (ParticleSystem)Instantiate (particleClickSoapOutside);
		partSystem.transform.position = obj.transform.position;
	}
}
