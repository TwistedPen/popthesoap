﻿/*
 * Author: Thais Thomas Borgholm
 */
using UnityEngine;
using System.Collections;

public class BubbleTrailParticles : MonoBehaviour {

	public ParticleSystem bubbleParticles;

	//keeps information about the particle system initial rotation
	private Quaternion parIniRot;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		//"freezes" the rotation on the particles that follow the soap
		if (this.transform.childCount > 0 && parIniRot != null) {
			this.transform.GetComponentInChildren<ParticleSystem>().transform.rotation = parIniRot;
				}
	}

	public void StartBubbles(GameObject obj)
	{	
		//Instantiates the particle system and add it as a child to the soap
		ParticleSystem partSystem = (ParticleSystem)Instantiate (bubbleParticles);
		parIniRot = partSystem.transform.rotation;
		partSystem.transform.parent = obj.transform;
		partSystem.transform.localPosition = Vector3.zero;
	}

	void DestroyAllBubbles(GameObject obj)
	{
		if (this.transform.childCount > 0) {
			//Destroy all ParticleSystems in childen when the soap is detroyed
			foreach (ParticleSystem pSys in this.transform.GetComponentsInChildren<ParticleSystem>()) {
					Destroy (pSys.gameObject);
			}
				
		}
	}

}
