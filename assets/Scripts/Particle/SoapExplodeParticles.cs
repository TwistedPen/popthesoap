﻿/*
 * Author: Thais Thomas Borgholm
 */
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoapExplodeParticles : MonoBehaviour {

	public ParticleSystem particleExplosion;
	public ParticleSystem particleDestroyedByExplotion;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ExplotionParticleStart (Vector3 pos, float diameter)
	{
		ParticleSystem partSystem = (ParticleSystem)Instantiate (particleExplosion);
		partSystem.transform.position = pos;
			partSystem.startSize = diameter;
	}

	public void DestroyedSoapInExplotionParticleStart(LinkedList<SoapDecay> destroyedList)
	{
		foreach (SoapDecay dScript in destroyedList) {
			ParticleSystem partSystem = (ParticleSystem)Instantiate (particleDestroyedByExplotion);
			partSystem.transform.position = dScript.gameObject.transform.position;
				}

	}

}
