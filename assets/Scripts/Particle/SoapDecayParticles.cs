﻿/*
 * Author: Thais Thomas Borgholm
 */
using UnityEngine;
using System.Collections;

public class SoapDecayParticles : MonoBehaviour
{

		public ParticleSystem particleDecaySoap;

		// Use this for initialization
		void Start ()
		{

		}
	
		// Update is called once per frame
		void Update ()
		{
		/*
		* if the particle system follows the soap use this:
		if (this.transform.childCount > 0) {
			//Check on all the particlesystems in child if they are alive and destroy if not
			foreach (ParticleSystem pSys in this.transform.GetComponentsInChildren<ParticleSystem>()) {
				if(!pSys.IsAlive ())
				{
					Destroy (pSys.gameObject);
				}
			}
		}
		*/
		}

		//Starts the Particle System on the Soap position
		public void DecayParticleStart (int status, GameObject obj)
		{
				if (status != 2) {
			ParticleSystem partSystem = (ParticleSystem)Instantiate (particleDecaySoap);
			/*
					 * if the particle system follows the soap use this:
					partSystem.transform.parent = this.transform;
					partSystem.transform.position = Vector3.zero;
					*/
						partSystem.transform.position = obj.transform.position;
				}
		/*TODO need explotions!!
		 * else if (status > 2) {
						ParticleSystem partSystem = (ParticleSystem)Instantiate (particleExplosion);
						partSystem.transform.position = obj.transform.position;
				}*/
		}


	
		void OnDestroy ()
		{
				/*
		* if the particle system follows the soap use this:
		if (this.transform.childCount > 0) {
			//Destroy all ParticleSystems in childen when the soap is detroyed
			foreach (ParticleSystem pSys in this.transform.GetComponentsInChildren<ParticleSystem>()) {
					Destroy (pSys.gameObject);
			}
				
		}
		*/

		}
}
