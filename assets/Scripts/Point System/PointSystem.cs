﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PointSystem : Singleton<PointSystem>
{
    public delegate void PointsChangedHandler(int pointDifference, Vector3? position);
    public event PointsChangedHandler PointsChanged;

	public delegate void PointThresholdExceededHandler(int threshold, ParticleSystem comboParticle);
	public event PointThresholdExceededHandler PointThresholdExceeded;

    // The point thresholds to determine when to display special feedback effects ("Sweet!", etc.)
    public int[] pointThresholds = new int[5];
    public ParticleSystem[] comboParticles = new ParticleSystem[5];
    public int pointsPerClick = 10;
    public int pointMultiplier = 100;

    public static int TotalPoints
    {
        get;
        private set;
    }

    public static Dictionary<SoapType.Type, int> SoapsDestroyedByType
    {
        get;
        private set;
    }

    private static PointSystemCalc PointSystemCalc = new PointSystemCalc();

    void Awake()
    {
        GameState.Instance.SoapChainExplosion += new GameState.SoapChainExplosionHandler(this.OnSoapChainExplosion);
        GameState.Instance.DestructibleClicked += new GameState.DestructibleClickedHandler(this.OnDestructibleClicked);
        PointSystem.SoapsDestroyedByType = new Dictionary<SoapType.Type, int>();
    }

    public static void Reset()
    {
        PointSystem.TotalPoints = 0;
        PointSystem.SoapsDestroyedByType[SoapType.Type.Normal] = 0;
        PointSystem.SoapsDestroyedByType[SoapType.Type.Red] = 0;
        PointSystem.SoapsDestroyedByType[SoapType.Type.Green] = 0;
    }

    void OnDestructibleClicked(GameObject destructible)
    {
        if(GreenAreaChecker.Instance.ObjectInArea(destructible))
        {
            if(destructible.GetComponent<DestructibleClicked>().givePoints)
            {
                this.AddPoints(this.pointsPerClick);
            }
        }
    }

    void OnSoapChainExplosion(SoapExplosionInfo explosionInfo)
    {
        int newPoints = this.pointMultiplier * PointSystem.PointSystemCalc.CalculatePoints(explosionInfo);

        PointSystem.SoapsDestroyedByType[SoapType.Type.Normal] += explosionInfo.soapsDestroyedByType[SoapType.Type.Normal];
        PointSystem.SoapsDestroyedByType[SoapType.Type.Red] += explosionInfo.soapsDestroyedByType[SoapType.Type.Red];
        PointSystem.SoapsDestroyedByType[SoapType.Type.Green] += explosionInfo.soapsDestroyedByType[SoapType.Type.Green];

        this.AddPoints(newPoints, explosionInfo);
    }

    void AddPoints(int newPoints, SoapExplosionInfo explosionInfo = null)
    {
        PointSystem.TotalPoints += newPoints;

        if(this.PointsChanged != null)
        {
            if(explosionInfo != null)
            {
                this.PointsChanged(newPoints, explosionInfo.firstSoapPosition);
            }
            else
            {
                this.PointsChanged(newPoints, null);
            }
        }

        if(this.PointThresholdExceeded != null)
        {
            int threshold = GetThresholdIndex(newPoints);

            if(threshold >= 0)
            {
                this.PointThresholdExceeded(threshold, comboParticles[threshold]);
            }
        }
    }

    /// <summary>
    /// Yields the point threshold for the given amount of points.
    /// </summary>
    /// <param name="points">The points to find within the thresholds.</param>
    /// <returns>The threshold index, which contains the given point amount.</returns>
    public int GetThresholdIndex(int points)
    {
        for(int index = this.pointThresholds.Length - 1; index >= 0; --index)
        {
            if(points >= pointThresholds[index])
            {
                return index;
            }
        }

        return -1;
    }
}
