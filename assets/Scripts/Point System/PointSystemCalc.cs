﻿/*
 * Author: Wojciech Terepeta & Martin Grunbaum
 */
using System.Collections;

/// <summary>
/// A basic point system that calculates the amount of points the player receives from explosions.
/// </summary>
public class PointSystemCalc
{

    /// <summary>
    /// Calculates the number of points from destroying soaps.
    /// </summary>
    /// <param name="explosionInfo">Detailed information about the explosion.</param>
    /// <returns>The number of points received from destroying the soaps.</returns>
    public int CalculatePoints(SoapExplosionInfo explosionInfo)
    {
        int destroyedSquared = explosionInfo.totalDestroyedSoaps * explosionInfo.totalDestroyedSoaps;
        // Destroyed ^ 2 + Exploded ^ 2
        int totalPoints = destroyedSquared + explosionInfo.totalExplodedSoaps * explosionInfo.totalExplodedSoaps;

        for(int i = 1, multiplier = 1, soapsTillPreviousExplosion = 0;
            i < explosionInfo.soapsDestroyedByEachExplosion.Count;
            ++i, ++multiplier)
        {
            soapsTillPreviousExplosion += explosionInfo.soapsDestroyedByEachExplosion[i - 1];
            int soapsTillCurrentExplosion = explosionInfo.soapsDestroyedByEachExplosion[i] + soapsTillPreviousExplosion;

            int explosionPoints = soapsTillCurrentExplosion * soapsTillCurrentExplosion - soapsTillPreviousExplosion * soapsTillPreviousExplosion;
            totalPoints += explosionPoints * multiplier;
        }

        return totalPoints;
    }
}
