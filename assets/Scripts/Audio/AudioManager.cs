﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour
{
	public bool isInGame = true;
		public float reliefWait = 0.6f;
		public float laughterHornyWait = 1.0f;
		public float randomFartDelayMin = 2.0f;
		public float randomFartDelayMax = 60.0f;
		public float airGuitarDelay = 0.25f;
	public float operaDelay = 0.25f;
	public float scrubbingDelay = 0.25f;

		// Use this for initialization
		void Start ()
		{
				if (isInGame) {
						StopAllSounds ();
						GameState.Instance.DestructibleClicked += new GameState.DestructibleClickedHandler (PlayThrowSound);
						GameState.Instance.Pause += new GameState.PauseHandler (PlayPauseMusic);
						GameState.Instance.GameOver += new GameState.GameOverHandler (PlayGameOverMusic);
						GameState.Instance.SoapChainExplosion += new GameState.SoapChainExplosionHandler (PlaySoapDestroySound);
						GameState.Instance.DecayStatus += new GameState.DecayStatusHandler (PlayDecaySound);
						GameState.Instance.Life += new GameState.LifeHandler (LifeChangeMusic);
						GameState.Instance.BackgroundAnimation += new GameState.BackgroundAnimationHandler (BackgroundAnimationSounds);
						GameState.Instance.ThrowSoap += new GameState.ThrowSoapHandler(PlayThrowHornySound);
						PointSystem.Instance.PointsChanged += new PointSystem.PointsChangedHandler (checkPoints);
						PlayMainMusic ();
						PlayHornSound ();
						PlayShower ();
						StartCoroutine (PlayRandomFartSound ());
						//StartCoroutine(test());
				}
		}

	void checkPoints(int pointsDiff, Vector3? pos)
			{
		if (PointSystem.TotalPoints % 2500 == 0) {
			PlayTwentyfivePoints ();
		} else if (PointSystem.TotalPoints % 1000 == 0) {
			PlayTenPoints ();
		}
		}

	void PlayThrowHornySound()
	{
		Fabric.EventManager.Instance.PostEvent ("ThrowSoapHorny", Fabric.EventAction.PlaySound);
		}

		void PlayHornyGuySound1 ()
		{
				Fabric.EventManager.Instance.PostEvent ("HornyGuy1", Fabric.EventAction.PlaySound);
		}

		void PlayHornyGuySound2 ()
		{
				Fabric.EventManager.Instance.PostEvent ("HornyGuy2", Fabric.EventAction.PlaySound);
		}

		void PlayHornyGuySound3 ()
		{
				Fabric.EventManager.Instance.PostEvent ("HornyGuy3", Fabric.EventAction.PlaySound);
		}

		void PlayThrowSound (GameObject obj)
		{
				Fabric.EventManager.Instance.PostEvent ("ThrowSoap", Fabric.EventAction.PlaySound);
		}

		void PlayMainMusic ()
		{
				Fabric.EventManager.Instance.PostEvent ("MainMusic", Fabric.EventAction.AdvanceSequence);
		}

		void LifeChangeMusic (int life)
		{
				Fabric.EventManager.Instance.PostEvent ("MainMusic", Fabric.EventAction.AdvanceSequence);
				StartCoroutine (PlayLifeLose ());
		}

		void PlayPauseMusic (bool state)
		{
				if (Fabric.EventManager.Instance != null) {
						if (state) {
								Fabric.EventManager.Instance.PostEvent ("MainMusic", Fabric.EventAction.PauseSound);
								Fabric.EventManager.Instance.PostEvent ("PauseMusic", Fabric.EventAction.PlaySound);
						} else {
								Fabric.EventManager.Instance.PostEvent ("PauseMusic", Fabric.EventAction.StopSound);
								Fabric.EventManager.Instance.PostEvent ("MainMusic", Fabric.EventAction.UnpauseSound);
						}
				}
		}

		void PlayShower ()
		{
				Fabric.EventManager.Instance.PostEvent ("BackgroundSounds", Fabric.EventAction.PlaySound);
		}

		void PlayGameOverMusic ()
		{
				Fabric.EventManager.Instance.PostEvent ("GameOverLaugh", Fabric.EventAction.PlaySound);
				Fabric.EventManager.Instance.PostEvent ("GameOverMusic", Fabric.EventAction.PlaySound);
		}

		void PlaySoapDestroySound (SoapExplosionInfo explosionInfo)
		{
				
				Fabric.EventManager.Instance.PostEvent ("Explotion", Fabric.EventAction.PlaySound);
				StartCoroutine (PlayPlayerRelief ());
		}
		
		void PlayDecaySound (int status, GameObject obj)
		{
				Fabric.EventManager.Instance.PostEvent ("SoapDecay", Fabric.EventAction.PlaySound);
		}

		void BackgroundAnimationSounds (int animation)
		{
				switch (animation) {
				case 0:
						StartCoroutine (PlayAirGuitarSound ());
						break;
				case 1:
						StartCoroutine (PlayScrubbingSound ());
						break;
				case 2:
						StartCoroutine (PlayScrubbingSound ());
						break;
				case 3:
						StartCoroutine (PlayOperaSound ());
						break;
				case 4:
						StartCoroutine (PlayScrubbingSound ());
						break;
				default:
						break;
				}
		}

		IEnumerator PlayOperaSound ()
		{
				//Debug.Log ("Opera!!!");
				yield return new WaitForSeconds (operaDelay);
				Fabric.EventManager.Instance.PostEvent ("Opera", Fabric.EventAction.PlaySound);
		Fabric.EventManager.Instance.PostEvent ("AirGuitar", Fabric.EventAction.StopSound);
		Fabric.EventManager.Instance.PostEvent ("Scrubbing", Fabric.EventAction.StopSound);
		
		}

		IEnumerator PlayAirGuitarSound ()
		{
				//Debug.Log ("Air guitar!!!");
				yield return new WaitForSeconds (airGuitarDelay);
				Fabric.EventManager.Instance.PostEvent ("AirGuitar", Fabric.EventAction.PlaySound);
				Fabric.EventManager.Instance.PostEvent ("Opera", Fabric.EventAction.StopSound);
			Fabric.EventManager.Instance.PostEvent ("Scrubbing", Fabric.EventAction.StopSound);

		}

		IEnumerator PlayScrubbingSound ()
		{
				//Debug.Log ("Scrubbing!!!");
				yield return new WaitForSeconds (scrubbingDelay);
				Fabric.EventManager.Instance.PostEvent ("Scrubbing", Fabric.EventAction.PlaySound);
		Fabric.EventManager.Instance.PostEvent ("Opera", Fabric.EventAction.StopSound);
		Fabric.EventManager.Instance.PostEvent ("AirGuitar", Fabric.EventAction.StopSound);
		
		}

		void PlayHornSound ()
		{
				Fabric.EventManager.Instance.PostEvent ("Horn", Fabric.EventAction.PlaySound);
		}

	void PlayTenPoints()
	{
		//Debug.Log ("1000Points");
		Fabric.EventManager.Instance.PostEvent ("1000points", Fabric.EventAction.PlaySound);
		}

	void PlayTwentyfivePoints()
	{
		//Debug.Log ("2500Points");
		Fabric.EventManager.Instance.PostEvent ("2500points", Fabric.EventAction.PlaySound);
	}

		IEnumerator PlayRandomFartSound ()
		{
				while (true) {
						float randTime = Random.Range (randomFartDelayMin, randomFartDelayMax);
						yield return new WaitForSeconds (randTime);
						Fabric.EventManager.Instance.PostEvent ("Fart", Fabric.EventAction.PlaySound);
				}
		}

		IEnumerator PlayLifeLose ()
		{
				Fabric.EventManager.Instance.PostEvent ("LifeChangePlayer", Fabric.EventAction.PlaySound);
				yield return new WaitForSeconds (laughterHornyWait);
				Fabric.EventManager.Instance.PostEvent ("LifeChangeHorny", Fabric.EventAction.PlaySound);

		}

		IEnumerator PlayPlayerRelief ()
		{
				yield return new WaitForSeconds (reliefWait);
				Fabric.EventManager.Instance.PostEvent ("PlayerRelief", Fabric.EventAction.PlaySound);

		}

		void OnDestroy()
		{
		try {
			//StopAllSounds ();
			//RemoveRelations ();
			//Destroy (this.gameObject);
		} catch (System.Exception ex) {
			
				}
				
		}

	void RemoveRelations()
	{
		GameState.Instance.DestructibleClicked -= new GameState.DestructibleClickedHandler (PlayThrowSound);
		GameState.Instance.Pause -= new GameState.PauseHandler (PlayPauseMusic);
		GameState.Instance.GameOver -= new GameState.GameOverHandler (PlayGameOverMusic);
		GameState.Instance.SoapChainExplosion -= new GameState.SoapChainExplosionHandler (PlaySoapDestroySound);
		GameState.Instance.DecayStatus -= new GameState.DecayStatusHandler (PlayDecaySound);
		GameState.Instance.Life -= new GameState.LifeHandler (LifeChangeMusic);
		GameState.Instance.BackgroundAnimation -= new GameState.BackgroundAnimationHandler (BackgroundAnimationSounds);
		PointSystem.Instance.PointsChanged -= new PointSystem.PointsChangedHandler (checkPoints);
	}
		void StopAllSounds ()
		{
				foreach (string eventName in Fabric.EventManager.Instance._eventList) {
			if(eventName == "MainMusic")
			{
				Fabric.EventManager.Instance.PostEvent ("MainMusic", Fabric.EventAction.PauseSound);
			}else{
						Fabric.EventManager.Instance.PostEvent (eventName, Fabric.EventAction.StopSound);
			}
				}	
		}


}
